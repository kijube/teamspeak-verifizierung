package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.api.Callback;
import com.github.theholywaffle.teamspeak3.api.exception.TS3ConnectionFailedException;
import com.github.theholywaffle.teamspeak3.api.wrapper.QueryError;
import com.github.theholywaffle.teamspeak3.commands.CQuit;
import com.github.theholywaffle.teamspeak3.commands.Command;
import com.github.theholywaffle.teamspeak3.log.LogHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Handler;
import java.util.logging.Logger;

public class TS3Query
{
  public static enum FloodRate
  {
    DEFAULT(350),  UNLIMITED(0);
    
    private final int ms;
    
    private FloodRate(int ms)
    {
      this.ms = ms;
    }
    
    public int getMs()
    {
      return this.ms;
    }
  }
  
  public static final Logger log = Logger.getLogger(TS3Query.class.getName());
  private final EventManager eventManager = new EventManager();
  private final TS3Config config;
  private Socket socket;
  private PrintStream out;
  private BufferedReader in;
  private SocketReader socketReader;
  private SocketWriter socketWriter;
  private KeepAliveThread keepAlive;
  private ConcurrentLinkedQueue<Command> commandList = new ConcurrentLinkedQueue();
  private TS3Api api;
  
  public TS3Query(TS3Config config)
  {
    log.setUseParentHandlers(false);
    log.addHandler(new LogHandler(config.getDebugToFile()));
    log.setLevel(config.getDebugLevel());
    this.config = config;
  }
  
  public TS3Query connect()
  {
    try
    {
      this.socket = new Socket(this.config.getHost(), this.config.getQueryPort());
      if (this.socket.isConnected())
      {
        this.out = new PrintStream(this.socket.getOutputStream(), true, "UTF-8");
        this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "UTF-8"));
        this.socketReader = new SocketReader(this);
        this.socketReader.start();
        this.socketWriter = new SocketWriter(this, this.config.getFloodRate().getMs());
        this.socketWriter.start();
        this.keepAlive = new KeepAliveThread(this, this.socketWriter);
        this.keepAlive.start();
      }
    }
    catch (IOException e)
    {
      throw new TS3ConnectionFailedException(e);
    }
    TS3Api api = getApi();
    if ((this.config.getUsername() != null) && (this.config.getPassword() != null)) {
      api.login(this.config.getUsername(), this.config.getPassword());
    }
    return this;
  }
  
  public Socket getSocket()
  {
    return this.socket;
  }
  
  public PrintStream getOut()
  {
    return this.out;
  }
  
  public BufferedReader getIn()
  {
    return this.in;
  }
  
  public boolean doCommand(Command c)
  {
    final Object signal = new Object();
    Callback callback = new Callback()
    {
      public void handle()
      {
        synchronized (signal)
        {
          signal.notifyAll();
        }
      }
    };
    this.socketReader.registerCallback(c, callback);
    
    long end = System.currentTimeMillis() + this.config.getCommandTimeout();
    this.commandList.offer(c);
    
    boolean interrupted = false;
    while ((!c.isAnswered()) && (System.currentTimeMillis() < end)) {
      try
      {
        synchronized (signal)
        {
          signal.wait(end - System.currentTimeMillis());
        }
      }
      catch (InterruptedException e)
      {
        interrupted = true;
      }
    }
    if (interrupted) {
      Thread.currentThread().interrupt();
    }
    if (!c.isAnswered())
    {
      log.severe("Command " + c.getName() + " was not answered in time.");
      return false;
    }
    return c.getError().isSuccessful();
  }
  
  public void doCommandAsync(Command c)
  {
    doCommandAsync(c, null);
  }
  
  public void doCommandAsync(Command c, Callback callback)
  {
    if (callback != null) {
      this.socketReader.registerCallback(c, callback);
    }
    this.commandList.offer(c);
  }
  
  public void exit()
  {
    doCommand(new CQuit());
    if (this.keepAlive != null) {
      this.keepAlive.interrupt();
    }
    if (this.socketWriter != null) {
      this.socketWriter.interrupt();
    }
    if (this.socketReader != null) {
      this.socketReader.interrupt();
    }
    if (this.out != null) {
      this.out.close();
    }
    if (this.in != null) {
      try
      {
        this.in.close();
      }
      catch (IOException ignored) {}
    }
    if (this.socket != null) {
      try
      {
        this.socket.close();
      }
      catch (IOException ignored) {}
    }
    try
    {
      if (this.keepAlive != null) {
        this.keepAlive.join();
      }
      if (this.socketWriter != null) {
        this.socketWriter.join();
      }
      if (this.socketReader != null) {
        this.socketReader.join();
      }
    }
    catch (InterruptedException e)
    {
      Thread.currentThread().interrupt();
    }
    this.commandList.clear();
    this.commandList = null;
    for (Handler lh : log.getHandlers()) {
      log.removeHandler(lh);
    }
  }
  
  public ConcurrentLinkedQueue<Command> getCommandList()
  {
    return this.commandList;
  }
  
  public EventManager getEventManager()
  {
    return this.eventManager;
  }
  
  public TS3Api getApi()
  {
    if (this.api == null) {
      this.api = new TS3Api(this);
    }
    return this.api;
  }
}
