package com.github.theholywaffle.teamspeak3;

import java.util.logging.Level;

public class TS3Config
{
  private String host = null;
  private int queryPort = 10011;
  private TS3Query.FloodRate floodRate = TS3Query.FloodRate.DEFAULT;
  private Level level = Level.WARNING;
  private String username = null;
  private String password = null;
  private boolean debugToFile = false;
  private int commandTimeout = 4000;
  
  public TS3Config setHost(String host)
  {
    this.host = host;
    return this;
  }
  
  String getHost()
  {
    return this.host;
  }
  
  public TS3Config setQueryPort(int queryPort)
  {
    this.queryPort = queryPort;
    return this;
  }
  
  int getQueryPort()
  {
    return this.queryPort;
  }
  
  public TS3Config setFloodRate(TS3Query.FloodRate rate)
  {
    this.floodRate = rate;
    return this;
  }
  
  TS3Query.FloodRate getFloodRate()
  {
    return this.floodRate;
  }
  
  public TS3Config setDebugLevel(Level level)
  {
    this.level = level;
    return this;
  }
  
  Level getDebugLevel()
  {
    return this.level;
  }
  
  public TS3Config setLoginCredentials(String username, String password)
  {
    this.username = username;
    this.password = password;
    return this;
  }
  
  String getUsername()
  {
    return this.username;
  }
  
  String getPassword()
  {
    return this.password;
  }
  
  public TS3Config setDebugToFile(boolean debugToFile)
  {
    this.debugToFile = debugToFile;
    return this;
  }
  
  boolean getDebugToFile()
  {
    return this.debugToFile;
  }
  
  public TS3Config setCommandTimeout(int commandTimeout)
  {
    if (commandTimeout <= 0) {
      throw new IllegalArgumentException("Timeout value must be greater than 0");
    }
    this.commandTimeout = commandTimeout;
    return this;
  }
  
  int getCommandTimeout()
  {
    return this.commandTimeout;
  }
}
