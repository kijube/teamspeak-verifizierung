package com.github.theholywaffle.teamspeak3.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class LogHandler
  extends Handler
{
  private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  private final boolean writeToFile;
  private final PrintWriter fileWriter;
  
  public LogHandler(boolean writeToFile)
  {
    boolean toFile = writeToFile;
    PrintWriter out = null;
    if (toFile) {
      try
      {
        File log = new File("teamspeak.log");
        if (!log.exists()) {
          log.createNewFile();
        }
        out = new PrintWriter(new BufferedWriter(new FileWriter(log, true)), true);
      }
      catch (IOException io)
      {
        System.err.println("Could not set log handler! Using System.out instead");
        io.printStackTrace();
        toFile = false;
      }
    }
    this.writeToFile = toFile;
    this.fileWriter = out;
  }
  
  public void close()
  {
    if (this.writeToFile)
    {
      this.fileWriter.flush();
      this.fileWriter.close();
    }
  }
  
  public void flush()
  {
    if (this.writeToFile) {
      this.fileWriter.flush();
    }
  }
  
  public void publish(LogRecord record)
  {
    StringBuilder logMessage = new StringBuilder();
    
    logMessage.append("[").append(format.format(new Date())).append("] ");
    if (record.getLevel().intValue() >= Level.WARNING.intValue()) {
      logMessage.append("[").append(record.getLevel()).append("] ");
    }
    logMessage.append(record.getMessage());
    
    System.out.println(logMessage);
    if (this.writeToFile) {
      this.fileWriter.println(logMessage);
    }
  }
}
