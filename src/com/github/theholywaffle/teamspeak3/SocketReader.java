package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.api.Callback;
import com.github.theholywaffle.teamspeak3.api.wrapper.QueryError;
import com.github.theholywaffle.teamspeak3.commands.Command;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketReader
  extends Thread
{
  private final TS3Query ts3;
  private final ExecutorService userThreadPool;
  private final Map<Command, Callback> callbackMap;
  private String lastEvent;
  
  public SocketReader(TS3Query ts3)
  {
    super("[TeamSpeak-3-Java-API] SocketReader");
    this.ts3 = ts3;
    this.userThreadPool = Executors.newCachedThreadPool();
    this.callbackMap = Collections.synchronizedMap(new LinkedHashMap());
    this.lastEvent = "";
    try
    {
      int i = 0;
      while ((i < 4) || (ts3.getIn().ready()))
      {
        TS3Query.log.info("< " + ts3.getIn().readLine());
        i++;
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  public void run()
  {
    while ((this.ts3.getSocket() != null) && (this.ts3.getSocket().isConnected()) && (this.ts3.getIn() != null) && (!isInterrupted()))
    {
      final String line;
      try
      {
        line = this.ts3.getIn().readLine();
      }
      catch (IOException io)
      {
        if (!isInterrupted()) {
          io.printStackTrace();
        }
        break;
      }
      if (line == null) {
        break;
      }
      if (!line.isEmpty())
      {
        Command c = (Command)this.ts3.getCommandList().peek();
        if (line.startsWith("notify"))
        {
          TS3Query.log.info("< [event] " + line);
          if (!isDuplicate(line)) {
            this.userThreadPool.execute(new Runnable()
            {
              public void run()
              {
                String[] arr = line.split(" ", 2);
                SocketReader.this.ts3.getEventManager().fireEvent(arr[0], arr[1]);
              }
            });
          }
        }
        else if ((c != null) && (c.isSent()))
        {
          TS3Query.log.info("[" + c.getName() + "] < " + line);
          if (line.startsWith("error"))
          {
            c.feedError(line.substring("error ".length()));
            if (c.getError().getId() != 0) {
              TS3Query.log.severe("TS3 command error: " + c.getError());
            }
            c.setAnswered();
            this.ts3.getCommandList().remove(c);
            answerCallback(c);
          }
          else
          {
            c.feed(line);
          }
        }
        else
        {
          TS3Query.log.warning("[UNHANDLED] < " + line);
        }
      }
    }
    this.userThreadPool.shutdown();
    if (!isInterrupted()) {
      TS3Query.log.warning("SocketReader has stopped!");
    }
  }
  
  private void answerCallback(Command c)
  {
    final Callback callback = (Callback)this.callbackMap.get(c);
    if (callback == null) {
      return;
    }
    Set<Command> keySet = this.callbackMap.keySet();
    synchronized (this.callbackMap)
    {
      Iterator<Command> iterator = keySet.iterator();
      while ((iterator.hasNext()) && (!c.equals(iterator.next()))) {
        iterator.remove();
      }
    }
    this.userThreadPool.execute(new Runnable()
    {
      public void run()
      {
        try
        {
          callback.handle();
        }
        catch (Throwable t)
        {
          TS3Query.log.log(Level.WARNING, "User callback threw exception", t);
        }
      }
    });
  }
  
  void registerCallback(Command command, Callback callback)
  {
    this.callbackMap.put(command, callback);
  }
  
  private boolean isDuplicate(String eventMessage)
  {
    if ((!eventMessage.startsWith("notifyclientmoved")) && (!eventMessage.startsWith("notifycliententerview")) && (!eventMessage.startsWith("notifyclientleftview"))) {
      return false;
    }
    if (eventMessage.equals(this.lastEvent))
    {
      this.lastEvent = "";
      return true;
    }
    this.lastEvent = eventMessage;
    return false;
  }
}
