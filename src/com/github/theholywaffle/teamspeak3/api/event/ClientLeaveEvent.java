package com.github.theholywaffle.teamspeak3.api.event;

import java.util.Map;

public class ClientLeaveEvent
  extends BaseEvent
{
  public ClientLeaveEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getClientFromId()
  {
    return getInt("cfid");
  }
  
  public int getClientTargetId()
  {
    return getInt("ctid");
  }
  
  public int getClientId()
  {
    return getInt("clid");
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onClientLeave(this);
  }
}
