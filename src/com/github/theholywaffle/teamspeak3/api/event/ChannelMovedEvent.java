package com.github.theholywaffle.teamspeak3.api.event;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import java.util.Map;

public class ChannelMovedEvent
  extends BaseEvent
{
  public ChannelMovedEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getChannelId()
  {
    return getInt(ChannelProperty.CID);
  }
  
  public int getChannelParentId()
  {
    return getInt(ChannelProperty.CPID);
  }
  
  public int getChannelOrder()
  {
    return getInt(ChannelProperty.CHANNEL_ORDER);
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onChannelMoved(this);
  }
}
