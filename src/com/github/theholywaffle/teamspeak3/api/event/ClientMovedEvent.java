package com.github.theholywaffle.teamspeak3.api.event;

import java.util.Map;

public class ClientMovedEvent
  extends BaseEvent
{
  public ClientMovedEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getClientTargetId()
  {
    return getInt("ctid");
  }
  
  public int getClientId()
  {
    return getInt("clid");
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onClientMoved(this);
  }
}
