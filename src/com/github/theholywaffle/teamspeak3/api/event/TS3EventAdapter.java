package com.github.theholywaffle.teamspeak3.api.event;

public abstract class TS3EventAdapter
  implements TS3Listener
{
  public void onTextMessage(TextMessageEvent e) {}
  
  public void onClientJoin(ClientJoinEvent e) {}
  
  public void onClientLeave(ClientLeaveEvent e) {}
  
  public void onServerEdit(ServerEditedEvent e) {}
  
  public void onChannelEdit(ChannelEditedEvent e) {}
  
  public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {}
  
  public void onClientMoved(ClientMovedEvent e) {}
  
  public void onChannelCreate(ChannelCreateEvent e) {}
  
  public void onChannelDeleted(ChannelDeletedEvent e) {}
  
  public void onChannelMoved(ChannelMovedEvent e) {}
  
  public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {}
}
