package com.github.theholywaffle.teamspeak3.api.event;

import com.github.theholywaffle.teamspeak3.api.wrapper.Wrapper;
import java.util.Map;

public abstract class BaseEvent
  extends Wrapper
  implements TS3Event
{
  protected BaseEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getInvokerId()
  {
    return getInt("invokerid");
  }
  
  public String getInvokerName()
  {
    return get("invokername");
  }
  
  public String getInvokerUniqueId()
  {
    return get("invokeruid");
  }
  
  public int getReasonId()
  {
    return getInt("reasonid");
  }
  
  public String getReasonMessage()
  {
    return get("reasonmsg");
  }
}
