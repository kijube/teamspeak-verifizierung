package com.github.theholywaffle.teamspeak3.api.event;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import java.util.Map;

public class ChannelPasswordChangedEvent
  extends BaseEvent
{
  public ChannelPasswordChangedEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getChannelId()
  {
    return getInt(ChannelProperty.CID);
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onChannelPasswordChanged(this);
  }
}
