package com.github.theholywaffle.teamspeak3.api.event;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import java.util.Map;

public class ChannelCreateEvent
  extends BaseEvent
{
  public ChannelCreateEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public int getChannelId()
  {
    return getInt(ChannelProperty.CID);
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onChannelCreate(this);
  }
}
