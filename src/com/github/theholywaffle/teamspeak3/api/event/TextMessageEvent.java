package com.github.theholywaffle.teamspeak3.api.event;

import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import java.util.Map;

public class TextMessageEvent
  extends BaseEvent
{
  public TextMessageEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public TextMessageTargetMode getTargetMode()
  {
    for (TextMessageTargetMode m : TextMessageTargetMode.values()) {
      if (m.getIndex() == getInt("targetmode")) {
        return m;
      }
    }
    return null;
  }
  
  public String getMessage()
  {
    return get("msg");
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onTextMessage(this);
  }
}
