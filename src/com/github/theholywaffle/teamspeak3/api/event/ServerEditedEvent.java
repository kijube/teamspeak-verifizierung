package com.github.theholywaffle.teamspeak3.api.event;

import java.util.Map;

public class ServerEditedEvent
  extends BaseEvent
{
  public ServerEditedEvent(Map<String, String> map)
  {
    super(map);
  }
  
  public void fire(TS3Listener listener)
  {
    listener.onServerEdit(this);
  }
}
