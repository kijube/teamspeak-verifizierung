package com.github.theholywaffle.teamspeak3.api.event;

public abstract interface TS3Event
{
  public abstract void fire(TS3Listener paramTS3Listener);
}
