package com.github.theholywaffle.teamspeak3.api.event;

public abstract interface TS3Listener
{
  public abstract void onTextMessage(TextMessageEvent paramTextMessageEvent);
  
  public abstract void onClientJoin(ClientJoinEvent paramClientJoinEvent);
  
  public abstract void onClientLeave(ClientLeaveEvent paramClientLeaveEvent);
  
  public abstract void onServerEdit(ServerEditedEvent paramServerEditedEvent);
  
  public abstract void onChannelEdit(ChannelEditedEvent paramChannelEditedEvent);
  
  public abstract void onChannelDescriptionChanged(ChannelDescriptionEditedEvent paramChannelDescriptionEditedEvent);
  
  public abstract void onClientMoved(ClientMovedEvent paramClientMovedEvent);
  
  public abstract void onChannelCreate(ChannelCreateEvent paramChannelCreateEvent);
  
  public abstract void onChannelDeleted(ChannelDeletedEvent paramChannelDeletedEvent);
  
  public abstract void onChannelMoved(ChannelMovedEvent paramChannelMovedEvent);
  
  public abstract void onChannelPasswordChanged(ChannelPasswordChangedEvent paramChannelPasswordChangedEvent);
}
