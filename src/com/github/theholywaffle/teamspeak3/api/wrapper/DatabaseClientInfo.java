package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import java.util.Map;

public class DatabaseClientInfo
  extends DatabaseClient
{
  public DatabaseClientInfo(Map<String, String> map)
  {
    super(map);
  }
  
  public int getDatabaseId()
  {
    return getInt(ClientProperty.CLIENT_DATABASE_ID);
  }
  
  public String getAvatar()
  {
    return get(ClientProperty.CLIENT_FLAG_AVATAR);
  }
  
  public long getMonthlyBytesUploaded()
  {
    return getLong(ClientProperty.CLIENT_MONTH_BYTES_UPLOADED);
  }
  
  public long getMonthlyBytesDownloaded()
  {
    return getLong(ClientProperty.CLIENT_MONTH_BYTES_DOWNLOADED);
  }
  
  public long getTotalBytesUploaded()
  {
    return getLong(ClientProperty.CLIENT_TOTAL_BYTES_UPLOADED);
  }
  
  public long getTotalBytesDownloaded()
  {
    return getLong(ClientProperty.CLIENT_TOTAL_BYTES_DOWNLOADED);
  }
  
  public long getIconId()
  {
    return getLong(ClientProperty.CLIENT_ICON_ID);
  }
  
  public String getBase64HashClientUID()
  {
    return get("client_base64HashClientUID");
  }
}
