package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.Codec;
import java.util.Map;

public abstract class ChannelBase
  extends Wrapper
{
  public ChannelBase(Map<String, String> map)
  {
    super(map);
  }
  
  public abstract int getId();
  
  public int getParentChannelId()
  {
    return getInt(ChannelProperty.PID);
  }
  
  public int getOrder()
  {
    return getInt(ChannelProperty.CHANNEL_ORDER);
  }
  
  public String getName()
  {
    return get(ChannelProperty.CHANNEL_NAME);
  }
  
  public String getTopic()
  {
    return get(ChannelProperty.CHANNEL_TOPIC);
  }
  
  public boolean isDefault()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_DEFAULT);
  }
  
  public boolean hasPassword()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_PASSWORD);
  }
  
  public boolean isPermanent()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_PERMANENT);
  }
  
  public boolean isSemiPermanent()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_SEMI_PERMANENT);
  }
  
  public Codec getCodec()
  {
    int codec = getInt(ChannelProperty.CHANNEL_CODEC);
    for (Codec c : Codec.values()) {
      if (c.getIndex() == codec) {
        return c;
      }
    }
    return Codec.UNKNOWN;
  }
  
  public int getCodecQuality()
  {
    return getInt(ChannelProperty.CHANNEL_CODEC_QUALITY);
  }
  
  public int getNeededTalkPower()
  {
    return getInt(ChannelProperty.CHANNEL_NEEDED_TALK_POWER);
  }
  
  public long getIconId()
  {
    return getLong(ChannelProperty.CHANNEL_ICON_ID);
  }
  
  public int getMaxClients()
  {
    return getInt(ChannelProperty.CHANNEL_MAXCLIENTS);
  }
  
  public int getMaxFamilyClients()
  {
    return getInt(ChannelProperty.CHANNEL_MAXFAMILYCLIENTS);
  }
}
