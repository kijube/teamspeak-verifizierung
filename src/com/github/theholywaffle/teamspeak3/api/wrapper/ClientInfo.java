package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import java.util.Map;

public class ClientInfo
  extends Client
{
  private final int clientId;
  
  public ClientInfo(int clientId, Map<String, String> map)
  {
    super(map);
    this.clientId = clientId;
  }
  
  public int getId()
  {
    return this.clientId;
  }
  
  public String getAvatar()
  {
    return get(ClientProperty.CLIENT_FLAG_AVATAR);
  }
  
  public long getBandwidthReceivedLastMinute()
  {
    return getLong(ClientProperty.CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_TOTAL);
  }
  
  public long getBandwidthReceivedLastSecond()
  {
    return getLong(ClientProperty.CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_TOTAL);
  }
  
  public long getBandwidthSentlastMinute()
  {
    return getLong(ClientProperty.CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_TOTAL);
  }
  
  public long getBandwidthSentLastSecond()
  {
    return getLong(ClientProperty.CONNECTION_BANDWIDTH_SENT_LAST_SECOND_TOTAL);
  }
  
  public String getBase64ClientUId()
  {
    return get("client_base64HashClientUID");
  }
  
  public int getDefaultChannel()
  {
    String channelId = get(ClientProperty.CLIENT_DEFAULT_CHANNEL);
    if (channelId.isEmpty()) {
      return -1;
    }
    return Integer.valueOf(channelId.substring(1)).intValue();
  }
  
  public String getDefaultToken()
  {
    return get(ClientProperty.CLIENT_DEFAULT_TOKEN);
  }
  
  public String getDescription()
  {
    return get(ClientProperty.CLIENT_DESCRIPTION);
  }
  
  public long getFiletransferBandwidthReceived()
  {
    return getLong(ClientProperty.CONNECTION_FILETRANSFER_BANDWIDTH_RECEIVED);
  }
  
  public long getFiletransferBandwidthSent()
  {
    return getLong(ClientProperty.CONNECTION_FILETRANSFER_BANDWIDTH_SENT);
  }
  
  public String getIp()
  {
    return get(ClientProperty.CONNECTION_CLIENT_IP);
  }
  
  public String getLoginName()
  {
    return get(ClientProperty.CLIENT_LOGIN_NAME);
  }
  
  public String getMetaData()
  {
    return get(ClientProperty.CLIENT_META_DATA);
  }
  
  public long getMonthlyBytesDownloaded()
  {
    return getLong(ClientProperty.CLIENT_MONTH_BYTES_DOWNLOADED);
  }
  
  public long getMonthlyBytesUploaded()
  {
    return getLong(ClientProperty.CLIENT_MONTH_BYTES_UPLOADED);
  }
  
  public int getNeededServerQueryViewPower()
  {
    return getInt(ClientProperty.CLIENT_NEEDED_SERVERQUERY_VIEW_POWER);
  }
  
  public String getPhoneticNickname()
  {
    return get(ClientProperty.CLIENT_NICKNAME_PHONETIC);
  }
  
  public String getTalkRequestMessage()
  {
    return get(ClientProperty.CLIENT_TALK_REQUEST_MSG);
  }
  
  public long getTimeConnected()
  {
    return getLong(ClientProperty.CONNECTION_CONNECTED_TIME);
  }
  
  public long getTotalBytesDownloaded()
  {
    return getLong(ClientProperty.CLIENT_TOTAL_BYTES_DOWNLOADED);
  }
  
  public long getTotalBytesReceived()
  {
    return getLong(ClientProperty.CONNECTION_BYTES_RECEIVED_TOTAL);
  }
  
  public long getTotalBytesSent()
  {
    return getLong(ClientProperty.CONNECTION_BYTES_SENT_TOTAL);
  }
  
  public long getTotalBytesUploaded()
  {
    return getLong(ClientProperty.CLIENT_TOTAL_BYTES_UPLOADED);
  }
  
  public int getTotalConnections()
  {
    return getInt(ClientProperty.CLIENT_TOTALCONNECTIONS);
  }
  
  public long getTotalPacketsReceived()
  {
    return getLong(ClientProperty.CONNECTION_PACKETS_RECEIVED_TOTAL);
  }
  
  public long getTotalPacketsSent()
  {
    return getLong(ClientProperty.CONNECTION_PACKETS_SENT_TOTAL);
  }
  
  public int getUnreadMessages()
  {
    return getInt(ClientProperty.CLIENT_UNREAD_MESSAGES);
  }
  
  public boolean isOutputOnlyMuted()
  {
    return getBoolean(ClientProperty.CLIENT_OUTPUTONLY_MUTED);
  }
  
  public boolean isRequestingToTalk()
  {
    return getBoolean(ClientProperty.CLIENT_TALK_REQUEST);
  }
  
  public boolean isTalking()
  {
    throw new UnsupportedOperationException();
  }
}
