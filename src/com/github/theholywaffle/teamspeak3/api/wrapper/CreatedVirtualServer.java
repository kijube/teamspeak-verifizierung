package com.github.theholywaffle.teamspeak3.api.wrapper;

import java.util.Map;

public class CreatedVirtualServer
  extends Wrapper
{
  public CreatedVirtualServer(Map<String, String> map)
  {
    super(map);
  }
  
  public int getId()
  {
    return getInt("sid");
  }
  
  public int getPort()
  {
    return getInt("virtualserver_port");
  }
  
  public String getServerAdminToken()
  {
    return get("token");
  }
}
