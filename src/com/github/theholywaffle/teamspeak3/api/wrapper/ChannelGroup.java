package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.PermissionGroupDatabaseType;
import java.util.Map;

public class ChannelGroup
  extends Wrapper
{
  public ChannelGroup(Map<String, String> map)
  {
    super(map);
  }
  
  public int getId()
  {
    return getInt("cgid");
  }
  
  @Deprecated
  public int getGroupId()
  {
    return getId();
  }
  
  public String getName()
  {
    return get("name");
  }
  
  public PermissionGroupDatabaseType getType()
  {
    int type = getInt("type");
    for (PermissionGroupDatabaseType t : PermissionGroupDatabaseType.values()) {
      if (t.getIndex() == type) {
        return t;
      }
    }
    return null;
  }
  
  public long getIconId()
  {
    return getLong("iconid");
  }
  
  public boolean isSavedInDatabase()
  {
    return getBoolean("savedb");
  }
  
  public int getSortId()
  {
    return getInt("sortid");
  }
  
  public int getNameMode()
  {
    return getInt("namemode");
  }
  
  public int getModifyPower()
  {
    return getInt("n_modifyp");
  }
  
  public int getMemberAddPower()
  {
    return getInt("n_member_addp");
  }
  
  public int getMemberRemovePower()
  {
    return getInt("n_member_removep");
  }
}
