package com.github.theholywaffle.teamspeak3.api.wrapper;

import java.util.Map;

public class Binding
  extends Wrapper
{
  public Binding(Map<String, String> map)
  {
    super(map);
  }
  
  public String getIp()
  {
    return get("ip");
  }
}
