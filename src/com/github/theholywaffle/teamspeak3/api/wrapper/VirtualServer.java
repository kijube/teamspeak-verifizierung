package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import com.github.theholywaffle.teamspeak3.api.VirtualServerStatus;
import java.util.Map;

public class VirtualServer
  extends Wrapper
{
  public VirtualServer(Map<String, String> map)
  {
    super(map);
  }
  
  public int getId()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_ID);
  }
  
  public int getPort()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_PORT);
  }
  
  public VirtualServerStatus getStatus()
  {
    String status = get(VirtualServerProperty.VIRTUALSERVER_STATUS);
    for (VirtualServerStatus s : VirtualServerStatus.values()) {
      if (status.equals(s.getName())) {
        return s;
      }
    }
    return VirtualServerStatus.UNKNOWN;
  }
  
  public int getClientsOnline()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_CLIENTSONLINE);
  }
  
  public int getQueryClientsOnline()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_QUERYCLIENTSONLINE);
  }
  
  public int getMaxClients()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_MAXCLIENTS);
  }
  
  public String getUniqueIdentifier()
  {
    return get(VirtualServerProperty.VIRTUALSERVER_UNIQUE_IDENTIFIER);
  }
  
  public long getUptime()
  {
    return getLong(VirtualServerProperty.VIRTUALSERVER_UPTIME);
  }
  
  public String getName()
  {
    return get(VirtualServerProperty.VIRTUALSERVER_NAME);
  }
  
  public boolean isAutoStart()
  {
    return getBoolean(VirtualServerProperty.VIRTUALSERVER_AUTOSTART);
  }
}
