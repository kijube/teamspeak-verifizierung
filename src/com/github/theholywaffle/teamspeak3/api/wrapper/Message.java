package com.github.theholywaffle.teamspeak3.api.wrapper;

import java.util.Date;
import java.util.Map;

public class Message
  extends Wrapper
{
  public Message(Map<String, String> map)
  {
    super(map);
  }
  
  public int getId()
  {
    return getInt("msgid");
  }
  
  public String getSenderUniqueIdentifier()
  {
    return get("cluid");
  }
  
  public String getSubject()
  {
    return get("subject");
  }
  
  public Date getReceivedDate()
  {
    return new Date(getLong("timestamp") * 1000L);
  }
  
  public boolean hasBeenRead()
  {
    return getBoolean("flag_read");
  }
}
