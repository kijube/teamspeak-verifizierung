package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.Property;
import java.util.Map;

public class Wrapper
{
  private final Map<String, String> map;
  
  public Wrapper(Map<String, String> map)
  {
    this.map = map;
  }
  
  public Map<String, String> getMap()
  {
    return this.map;
  }
  
  public boolean getBoolean(String propertyName)
  {
    return getInt(propertyName) == 1;
  }
  
  public boolean getBoolean(Property property)
  {
    return getBoolean(property.getName());
  }
  
  public double getDouble(String propertyName)
  {
    String value = get(propertyName);
    if ((value == null) || (value.isEmpty())) {
      return -1.0D;
    }
    return Double.valueOf(value).doubleValue();
  }
  
  public double getDouble(Property property)
  {
    return getDouble(property.getName());
  }
  
  public long getLong(String propertyName)
  {
    String value = get(propertyName);
    if ((value == null) || (value.isEmpty())) {
      return -1L;
    }
    return Long.valueOf(value).longValue();
  }
  
  public long getLong(Property property)
  {
    return getLong(property.getName());
  }
  
  public int getInt(String propertyName)
  {
    String value = get(propertyName);
    if ((value == null) || (value.isEmpty())) {
      return -1;
    }
    return Integer.valueOf(value).intValue();
  }
  
  public int getInt(Property property)
  {
    return getInt(property.getName());
  }
  
  public String get(String propertyName)
  {
    String result = (String)this.map.get(propertyName);
    return result != null ? result : "";
  }
  
  public String get(Property property)
  {
    return get(property.getName());
  }
  
  public String toString()
  {
    return this.map.toString();
  }
}
