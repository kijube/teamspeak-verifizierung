package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import com.github.theholywaffle.teamspeak3.api.VirtualServerStatus;
import java.util.Map;

public class ServerQueryInfo
  extends Wrapper
{
  public ServerQueryInfo(Map<String, String> map)
  {
    super(map);
  }
  
  public int getChannelId()
  {
    return getInt("client_channel_id");
  }
  
  public int getDatabaseId()
  {
    return getInt(ClientProperty.CLIENT_DATABASE_ID);
  }
  
  public int getId()
  {
    return getInt("client_id");
  }
  
  public String getLoginName()
  {
    return get(ClientProperty.CLIENT_LOGIN_NAME);
  }
  
  public String getNickname()
  {
    return get(ClientProperty.CLIENT_NICKNAME);
  }
  
  public int getOriginServerId()
  {
    return getInt("client_origin_server_id");
  }
  
  public String getUniqueIdentifier()
  {
    return get(ClientProperty.CLIENT_UNIQUE_IDENTIFIER);
  }
  
  public int getVirtualServerId()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_ID);
  }
  
  public int getVirtualServerPort()
  {
    return getInt(VirtualServerProperty.VIRTUALSERVER_PORT);
  }
  
  public VirtualServerStatus getVirtualServerStatus()
  {
    String status = get(VirtualServerProperty.VIRTUALSERVER_STATUS);
    for (VirtualServerStatus s : VirtualServerStatus.values()) {
      if (s.getName().equals(status)) {
        return s;
      }
    }
    return VirtualServerStatus.UNKNOWN;
  }
  
  public String getVirtualServerUniqueIdentifier()
  {
    return get(VirtualServerProperty.VIRTUALSERVER_UNIQUE_IDENTIFIER);
  }
}
