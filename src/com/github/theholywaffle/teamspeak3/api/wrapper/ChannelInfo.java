package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import java.util.Map;

public class ChannelInfo
  extends ChannelBase
{
  private final int channelId;
  
  public ChannelInfo(int channelId, Map<String, String> map)
  {
    super(map);
    this.channelId = channelId;
  }
  
  public int getId()
  {
    return this.channelId;
  }
  
  public String getDescription()
  {
    return get(ChannelProperty.CHANNEL_DESCRIPTION);
  }
  
  public String getPassword()
  {
    return get(ChannelProperty.CHANNEL_PASSWORD);
  }
  
  public int getCodecLatencyFactor()
  {
    return getInt(ChannelProperty.CHANNEL_CODEC_LATENCY_FACTOR);
  }
  
  public boolean isEncrypted()
  {
    return !getBoolean(ChannelProperty.CHANNEL_CODEC_IS_UNENCRYPTED);
  }
  
  public boolean hasUnlimitedClients()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED);
  }
  
  public boolean hasUnlimitedFamilyClients()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_MAXFAMILYCLIENTS_UNLIMITED);
  }
  
  public boolean hasInheritedMaxFamilyClients()
  {
    return getBoolean(ChannelProperty.CHANNEL_FLAG_MAXFAMILYCLIENTS_INHERITED);
  }
  
  public String getFilePath()
  {
    return get(ChannelProperty.CHANNEL_FILEPATH);
  }
  
  public boolean isForcedSilence()
  {
    return getBoolean(ChannelProperty.CHANNEL_FORCED_SILENCE);
  }
  
  public String getPhoneticName()
  {
    return get(ChannelProperty.CHANNEL_NAME_PHONETIC);
  }
}
