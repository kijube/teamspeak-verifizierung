package com.github.theholywaffle.teamspeak3.api.wrapper;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import java.util.Map;

public class Channel
  extends ChannelBase
{
  public Channel(Map<String, String> map)
  {
    super(map);
  }
  
  public int getId()
  {
    return getInt(ChannelProperty.CID);
  }
  
  public int getTotalClientsFamily()
  {
    return getInt("total_clients_family");
  }
  
  public int getTotalClients()
  {
    return getInt("total_clients");
  }
  
  public int getNeededSubscribePower()
  {
    return getInt(ChannelProperty.CHANNEL_NEEDED_SUBSCRIBE_POWER);
  }
}
