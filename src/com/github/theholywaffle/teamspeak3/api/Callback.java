package com.github.theholywaffle.teamspeak3.api;

public abstract interface Callback
{
  public abstract void handle();
}
