package com.github.theholywaffle.teamspeak3.api;

public class Snapshot
{
  private final String snapshot;
  
  public Snapshot(String snapshot)
  {
    this.snapshot = snapshot;
  }
  
  public String get()
  {
    return this.snapshot;
  }
}
