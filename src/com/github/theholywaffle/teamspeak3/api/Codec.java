package com.github.theholywaffle.teamspeak3.api;

public enum Codec
{
  SPEEX_NARROWBAND(0),  SPEEX_WIDEBAND(1),  SPEEX_ULTRAWIDEBAND(2),  CELT_MONO(3),  OPUS_VOICE(4),  OPUS_MUSIC(5),  UNKNOWN(-1);
  
  private final int i;
  
  private Codec(int i)
  {
    this.i = i;
  }
  
  public int getIndex()
  {
    return this.i;
  }
}
