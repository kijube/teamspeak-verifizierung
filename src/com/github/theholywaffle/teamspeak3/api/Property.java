package com.github.theholywaffle.teamspeak3.api;

public abstract interface Property
{
  public abstract String getName();
}
