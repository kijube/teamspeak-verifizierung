package com.github.theholywaffle.teamspeak3.api.exception;

public class TS3UnknownEventException
  extends TS3Exception
{
  private static final long serialVersionUID = -9179157153557357715L;
  
  public TS3UnknownEventException(String event)
  {
    super(event + " [Please report this exception to the developer!]");
  }
}
