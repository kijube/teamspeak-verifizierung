package com.github.theholywaffle.teamspeak3.api.exception;

import com.github.theholywaffle.teamspeak3.api.wrapper.QueryError;

public class TS3CommandFailedException
  extends TS3Exception
{
  private static final long serialVersionUID = 8179203326662268882L;
  private final QueryError queryError;
  
  public TS3CommandFailedException(QueryError error)
  {
    super(buildMessage(error));
    this.queryError = error;
  }
  
  private static String buildMessage(QueryError error)
  {
    StringBuilder msg = new StringBuilder("A command returned with a server error.\n");
    msg.append(error.getMessage()).append(" (ID ").append(error.getId()).append(")");
    
    String extra = error.getExtraMessage();
    if ((extra != null) && (!extra.isEmpty())) {
      msg.append(": ").append(extra);
    }
    int failedPermissionId = error.getFailedPermissionId();
    if (failedPermissionId > 0) {
      msg.append(", failed permission with ID ").append(failedPermissionId);
    }
    return msg.toString();
  }
  
  public QueryError getError()
  {
    return this.queryError;
  }
}
