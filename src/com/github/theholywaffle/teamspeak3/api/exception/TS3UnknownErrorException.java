package com.github.theholywaffle.teamspeak3.api.exception;

public class TS3UnknownErrorException
  extends TS3Exception
{
  private static final long serialVersionUID = -8458508268312921713L;
  
  public TS3UnknownErrorException(String error)
  {
    super(error + " [Please report this exception to the developer!]");
  }
}
