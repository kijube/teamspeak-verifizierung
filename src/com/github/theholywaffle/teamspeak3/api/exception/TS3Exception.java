package com.github.theholywaffle.teamspeak3.api.exception;

public abstract class TS3Exception
  extends RuntimeException
{
  private static final long serialVersionUID = 7167169981592989359L;
  
  protected TS3Exception(String msg)
  {
    super(msg);
  }
  
  protected TS3Exception(String msg, Throwable cause)
  {
    super(msg, cause);
  }
}
