package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.PermissionGroupDatabaseType;
import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;

public class CChannelGroupCopy
  extends Command
{
  public CChannelGroupCopy(int sourceGroupId, int targetGroupId, PermissionGroupDatabaseType type)
  {
    this(sourceGroupId, targetGroupId, "name", type);
  }
  
  public CChannelGroupCopy(int sourceGroupId, String groupName, PermissionGroupDatabaseType type)
  {
    this(sourceGroupId, 0, groupName, type);
  }
  
  private CChannelGroupCopy(int sourceGroupId, int targetGroupId, String groupName, PermissionGroupDatabaseType type)
  {
    super("channelgroupcopy");
    add(new KeyValueParam("scgid", sourceGroupId));
    add(new KeyValueParam("tcgid", targetGroupId));
    add(new KeyValueParam("name", groupName));
    add(new KeyValueParam("type", type.getIndex()));
  }
}
