package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;

public class CChannelMove
  extends Command
{
  public CChannelMove(int channelId, int channelParentId, int order)
  {
    super("channelmove");
    add(new KeyValueParam("cid", channelId));
    add(new KeyValueParam("cpid", channelParentId));
    add(new KeyValueParam("order", order < 0 ? 0 : order));
  }
}
