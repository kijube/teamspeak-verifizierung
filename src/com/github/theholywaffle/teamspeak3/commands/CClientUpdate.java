package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;
import java.util.Map;

public class CClientUpdate
  extends Command
{
  public CClientUpdate(Map<ClientProperty, String> options)
  {
    super("clientupdate");
    if (options != null) {
      for (ClientProperty p : options.keySet())
      {
        if (!p.isChangeable()) {
          throw new IllegalArgumentException("ClientProperty " + p.getName() + " is not changeable!");
        }
        add(new KeyValueParam(p.getName(), (String)options.get(p)));
      }
    }
  }
}
