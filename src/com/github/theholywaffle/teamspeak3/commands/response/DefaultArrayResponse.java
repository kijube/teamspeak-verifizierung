package com.github.theholywaffle.teamspeak3.commands.response;

import com.github.theholywaffle.teamspeak3.api.wrapper.Wrapper;
import com.github.theholywaffle.teamspeak3.commands.CommandEncoding;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class DefaultArrayResponse
{
  private final List<Wrapper> array;
  
  public DefaultArrayResponse()
  {
    this.array = Collections.emptyList();
  }
  
  public DefaultArrayResponse(String raw)
  {
    StringTokenizer tkn = new StringTokenizer(raw, "|", false);
    this.array = new LinkedList();
    while (tkn.hasMoreTokens())
    {
      Wrapper wrapper = new Wrapper(parse(tkn.nextToken()));
      this.array.add(wrapper);
    }
  }
  
  private Map<String, String> parse(String raw)
  {
    StringTokenizer st = new StringTokenizer(raw, " ", false);
    Map<String, String> options = new HashMap();
    while (st.hasMoreTokens())
    {
      String tmp = st.nextToken();
      int pos = tmp.indexOf("=");
      if (pos == -1)
      {
        String valuelessKey = CommandEncoding.decode(tmp);
        options.put(valuelessKey, "");
      }
      else
      {
        String key = CommandEncoding.decode(tmp.substring(0, pos));
        String value = CommandEncoding.decode(tmp.substring(pos + 1));
        options.put(key, value);
      }
    }
    return options;
  }
  
  public List<Wrapper> getArray()
  {
    return this.array;
  }
  
  public Wrapper getFirstResponse()
  {
    if (this.array.size() == 0) {
      return null;
    }
    return (Wrapper)this.array.get(0);
  }
  
  public String toString()
  {
    StringBuilder str = new StringBuilder();
    for (Wrapper wrapper : this.array) {
      str.append(wrapper.getMap()).append(" | ");
    }
    str.setLength(str.length() - " | ".length());
    return str.toString();
  }
}
