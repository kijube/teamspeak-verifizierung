package com.github.theholywaffle.teamspeak3.commands;

public final class CommandEncoding
{
  public static String encode(String str)
  {
    str = str.replace("\\", "\\\\");
    
    str = str.replace(" ", "\\s");
    str = str.replace("/", "\\/");
    str = str.replace("|", "\\p");
    str = str.replace("\b", "\\b");
    str = str.replace("\f", "\\f");
    str = str.replace("\n", "\\n");
    str = str.replace("\r", "\\r");
    str = str.replace("\t", "\\t");
    str = str.replace(String.valueOf('\007'), "\\a");
    str = str.replace(String.valueOf('\013'), "\\v");
    
    return str;
  }
  
  public static String decode(String str)
  {
    str = str.replace("\\s", " ");
    str = str.replace("\\/", "/");
    str = str.replace("\\p", "|");
    str = str.replace("\\b", "\b");
    str = str.replace("\\f", "\f");
    str = str.replace("\\n", "\n");
    str = str.replace("\\r", "\r");
    str = str.replace("\\t", "\t");
    str = str.replace("\\a", String.valueOf('\007'));
    str = str.replace("\\v", String.valueOf('\013'));
    
    str = str.replace("\\\\", "\\");
    
    return str;
  }
}
