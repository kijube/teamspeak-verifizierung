package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;
import java.util.Map;

public class CChannelCreate
  extends Command
{
  public CChannelCreate(String name, Map<ChannelProperty, String> options)
  {
    super("channelcreate");
    add(new KeyValueParam("channel_name", name));
    if (options != null) {
      for (ChannelProperty p : options.keySet())
      {
        if (!p.isChangeable()) {
          throw new IllegalArgumentException("ChannelProperty " + p.getName() + " is not changeable!");
        }
        add(new KeyValueParam(p.getName(), (String)options.get(p)));
      }
    }
  }
}
