package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;

public class CClientMove
  extends Command
{
  public CClientMove(int clientId, int channelId, String channelPassword)
  {
    super("clientmove");
    add(new KeyValueParam("clid", clientId));
    add(new KeyValueParam("cid", channelId));
    if (channelPassword != null) {
      add(new KeyValueParam("cpw", channelPassword));
    }
  }
}
