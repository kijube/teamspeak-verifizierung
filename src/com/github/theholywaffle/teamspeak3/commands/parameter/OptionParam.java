package com.github.theholywaffle.teamspeak3.commands.parameter;

import com.github.theholywaffle.teamspeak3.commands.CommandEncoding;

public class OptionParam
  extends Parameter
{
  private final String option;
  
  public OptionParam(String option)
  {
    this.option = option;
  }
  
  public String build()
  {
    return "-" + CommandEncoding.encode(this.option);
  }
}
