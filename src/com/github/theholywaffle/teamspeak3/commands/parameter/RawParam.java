package com.github.theholywaffle.teamspeak3.commands.parameter;

public class RawParam
  extends Parameter
{
  private final String raw;
  
  public RawParam(String raw)
  {
    this.raw = raw;
  }
  
  public String build()
  {
    return this.raw;
  }
}
