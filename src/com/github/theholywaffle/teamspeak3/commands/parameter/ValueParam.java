package com.github.theholywaffle.teamspeak3.commands.parameter;

import com.github.theholywaffle.teamspeak3.commands.CommandEncoding;

public class ValueParam
  extends Parameter
{
  private final String value;
  
  public ValueParam(String value)
  {
    this.value = value;
  }
  
  public String build()
  {
    return CommandEncoding.encode(this.value);
  }
}
