package com.github.theholywaffle.teamspeak3.commands.parameter;

import com.github.theholywaffle.teamspeak3.commands.CommandEncoding;

public class KeyValueParam
  extends Parameter
{
  private final String key;
  private final String value;
  
  public KeyValueParam(String key, String value)
  {
    this.key = key;
    this.value = value;
  }
  
  public KeyValueParam(String key, int value)
  {
    this(key, String.valueOf(value));
  }
  
  public KeyValueParam(String key, long value)
  {
    this(key, String.valueOf(value));
  }
  
  public KeyValueParam(String key, boolean value)
  {
    this(key, value ? "1" : "0");
  }
  
  public String build()
  {
    return CommandEncoding.encode(this.key) + "=" + CommandEncoding.encode(this.value);
  }
}
