package com.github.theholywaffle.teamspeak3.commands.parameter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArrayParameter
  extends Parameter
{
  private final List<Parameter> parameters;
  
  public ArrayParameter()
  {
    this.parameters = new LinkedList();
  }
  
  public ArrayParameter(Parameter... parameters)
  {
    this.parameters = new LinkedList(Arrays.asList(parameters));
  }
  
  public ArrayParameter add(Parameter p)
  {
    this.parameters.add(p);
    return this;
  }
  
  public String build()
  {
    StringBuilder str = new StringBuilder();
    for (Parameter parameter : this.parameters) {
      str.append(parameter.build()).append("|");
    }
    str.setLength(str.length() - 1);
    return str.toString();
  }
}
