package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.wrapper.QueryError;
import com.github.theholywaffle.teamspeak3.api.wrapper.Wrapper;
import com.github.theholywaffle.teamspeak3.commands.parameter.Parameter;
import com.github.theholywaffle.teamspeak3.commands.response.DefaultArrayResponse;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class Command
{
  private final String command;
  private final List<Parameter> params = new LinkedList();
  private boolean sent = false;
  private boolean answered = false;
  private DefaultArrayResponse response;
  private QueryError error;
  private String raw;
  
  protected Command(String command)
  {
    this.command = command;
  }
  
  protected void add(Parameter p)
  {
    this.params.add(p);
  }
  
  public void feed(String str)
  {
    this.raw = str;
    if (this.response == null) {
      this.response = new DefaultArrayResponse(str);
    }
  }
  
  public void feedError(String err)
  {
    if (this.error == null)
    {
      DefaultArrayResponse errorResponse = new DefaultArrayResponse(err);
      this.error = new QueryError(errorResponse.getFirstResponse().getMap());
    }
  }
  
  public QueryError getError()
  {
    return this.error;
  }
  
  public String getName()
  {
    return this.command;
  }
  
  public Wrapper getFirstResponse()
  {
    if ((this.response == null) || (this.response.getArray().isEmpty())) {
      return new Wrapper(Collections.emptyMap());
    }
    return this.response.getFirstResponse();
  }
  
  public List<Wrapper> getResponse()
  {
    if (this.response == null) {
      return Collections.emptyList();
    }
    return this.response.getArray();
  }
  
  public boolean isAnswered()
  {
    return this.answered;
  }
  
  public boolean isSent()
  {
    return this.sent;
  }
  
  public void setAnswered()
  {
    this.answered = true;
  }
  
  public void setSent()
  {
    this.sent = true;
  }
  
  public String toString()
  {
    String str = this.command;
    for (Parameter p : this.params) {
      str = str + " " + p;
    }
    return str;
  }
  
  public String getRaw()
  {
    return this.raw;
  }
}
