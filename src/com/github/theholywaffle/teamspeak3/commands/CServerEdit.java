package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;
import java.util.Map;

public class CServerEdit
  extends Command
{
  public CServerEdit(Map<VirtualServerProperty, String> map)
  {
    super("serveredit");
    for (VirtualServerProperty p : map.keySet())
    {
      if (!p.isChangeable()) {
        throw new IllegalArgumentException("VirtualServerProperty " + p.getName() + " is not changeable!");
      }
      add(new KeyValueParam(p.getName(), (String)map.get(p)));
    }
  }
}
