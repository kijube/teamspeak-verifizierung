package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;
import java.util.Map;

public class CClientDBEdit
  extends Command
{
  public CClientDBEdit(int clientDBId, Map<ClientProperty, String> options)
  {
    super("clientdbedit");
    add(new KeyValueParam("cldbid", clientDBId));
    if (options != null) {
      for (ClientProperty p : options.keySet())
      {
        if (!p.isChangeable()) {
          throw new IllegalArgumentException("ClientProperty " + p.getName() + " is not changeable!");
        }
        add(new KeyValueParam(p.getName(), (String)options.get(p)));
      }
    }
  }
}
