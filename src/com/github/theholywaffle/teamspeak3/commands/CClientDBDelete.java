package com.github.theholywaffle.teamspeak3.commands;

import com.github.theholywaffle.teamspeak3.commands.parameter.KeyValueParam;

public class CClientDBDelete
  extends Command
{
  public CClientDBDelete(int clientDBId)
  {
    super("clientdbdelete");
    add(new KeyValueParam("cldbid", clientDBId));
  }
}
