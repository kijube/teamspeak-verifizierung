package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Event;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.exception.TS3UnknownEventException;
import com.github.theholywaffle.teamspeak3.api.wrapper.Wrapper;
import com.github.theholywaffle.teamspeak3.commands.response.DefaultArrayResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EventManager
{
  private final List<TS3Listener> listeners = new LinkedList();
  
  public void addListeners(TS3Listener... listeners)
  {
    for (TS3Listener l : listeners) {
      this.listeners.add(l);
    }
  }
  
  public void removeListeners(TS3Listener... listeners)
  {
    for (TS3Listener l : listeners) {
      this.listeners.remove(l);
    }
  }
  
  public void fireEvent(String notifyName, String notifyBody)
  {
    TS3Event event = createEvent(notifyName, notifyBody);
    for (TS3Listener listener : this.listeners) {
      event.fire(listener);
    }
  }
  
  private TS3Event createEvent(String notifyName, String notifyBody)
  {
    DefaultArrayResponse response = new DefaultArrayResponse(notifyBody);
    Map<String, String> eventData = response.getFirstResponse().getMap();
    switch (notifyName)
    {
    case "notifytextmessage": 
      return new TextMessageEvent(eventData);
    case "notifycliententerview": 
      return new ClientJoinEvent(eventData);
    case "notifyclientleftview": 
      return new ClientLeaveEvent(eventData);
    case "notifyserveredited": 
      return new ServerEditedEvent(eventData);
    case "notifychanneledited": 
      return new ChannelEditedEvent(eventData);
    case "notifychanneldescriptionchanged": 
      return new ChannelDescriptionEditedEvent(eventData);
    case "notifyclientmoved": 
      return new ClientMovedEvent(eventData);
    case "notifychannelcreated": 
      return new ChannelCreateEvent(eventData);
    case "notifychanneldeleted": 
      return new ChannelDeletedEvent(eventData);
    case "notifychannelmoved": 
      return new ChannelMovedEvent(eventData);
    case "notifychannelpasswordchanged": 
      return new ChannelPasswordChangedEvent(eventData);
    }
    throw new TS3UnknownEventException(notifyName + " " + notifyBody);
  }
}
