package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.commands.Command;
import java.io.PrintStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

public class SocketWriter
  extends Thread
{
  private final TS3Query ts3;
  private final int floodRate;
  private volatile long lastCommand = System.currentTimeMillis();
  
  public SocketWriter(TS3Query ts3, int floodRate)
  {
    super("[TeamSpeak-3-Java-API] SocketWriter");
    this.ts3 = ts3;
    if (floodRate > 50) {
      this.floodRate = floodRate;
    } else {
      this.floodRate = 50;
    }
  }
  
  public void run()
  {
    while ((this.ts3.getSocket() != null) && (this.ts3.getSocket().isConnected()) && (this.ts3.getOut() != null) && (!isInterrupted()))
    {
      Command c = (Command)this.ts3.getCommandList().peek();
      if ((c != null) && (!c.isSent()))
      {
        String msg = c.toString();
        TS3Query.log.info("> " + msg);
        
        c.setSent();
        this.ts3.getOut().println(msg);
        this.lastCommand = System.currentTimeMillis();
      }
      try
      {
        Thread.sleep(this.floodRate);
      }
      catch (InterruptedException e)
      {
        interrupt();
        break;
      }
    }
    if (!isInterrupted()) {
      TS3Query.log.warning("SocketWriter has stopped!");
    }
  }
  
  public long getIdleTime()
  {
    return System.currentTimeMillis() - this.lastCommand;
  }
}
