package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.PermissionGroupDatabaseType;
import com.github.theholywaffle.teamspeak3.api.ReasonIdentifier;
import com.github.theholywaffle.teamspeak3.api.ServerGroupType;
import com.github.theholywaffle.teamspeak3.api.ServerInstanceProperty;
import com.github.theholywaffle.teamspeak3.api.Snapshot;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.TokenType;
import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.wrapper.AdvancedPermission;
import com.github.theholywaffle.teamspeak3.api.wrapper.Ban;
import com.github.theholywaffle.teamspeak3.api.wrapper.Binding;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelBase;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelGroup;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelGroupClient;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Complaint;
import com.github.theholywaffle.teamspeak3.api.wrapper.ConnectionInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.CreatedVirtualServer;
import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClient;
import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.HostInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.InstanceInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Message;
import com.github.theholywaffle.teamspeak3.api.wrapper.Permission;
import com.github.theholywaffle.teamspeak3.api.wrapper.PermissionInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.PrivilegeKey;
import com.github.theholywaffle.teamspeak3.api.wrapper.QueryError;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroupClient;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerQueryInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Version;
import com.github.theholywaffle.teamspeak3.api.wrapper.VirtualServer;
import com.github.theholywaffle.teamspeak3.api.wrapper.VirtualServerInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Wrapper;
import com.github.theholywaffle.teamspeak3.commands.CBanAdd;
import com.github.theholywaffle.teamspeak3.commands.CBanClient;
import com.github.theholywaffle.teamspeak3.commands.CBanDel;
import com.github.theholywaffle.teamspeak3.commands.CBanDelAll;
import com.github.theholywaffle.teamspeak3.commands.CBanList;
import com.github.theholywaffle.teamspeak3.commands.CBindingList;
import com.github.theholywaffle.teamspeak3.commands.CChannelAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelClientAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelClientDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelClientPermList;
import com.github.theholywaffle.teamspeak3.commands.CChannelCreate;
import com.github.theholywaffle.teamspeak3.commands.CChannelDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelDelete;
import com.github.theholywaffle.teamspeak3.commands.CChannelEdit;
import com.github.theholywaffle.teamspeak3.commands.CChannelFind;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupAdd;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupClientList;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupCopy;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupDel;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupList;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupPermList;
import com.github.theholywaffle.teamspeak3.commands.CChannelGroupRename;
import com.github.theholywaffle.teamspeak3.commands.CChannelInfo;
import com.github.theholywaffle.teamspeak3.commands.CChannelList;
import com.github.theholywaffle.teamspeak3.commands.CChannelMove;
import com.github.theholywaffle.teamspeak3.commands.CChannelPermList;
import com.github.theholywaffle.teamspeak3.commands.CClientAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CClientDBDelete;
import com.github.theholywaffle.teamspeak3.commands.CClientDBEdit;
import com.github.theholywaffle.teamspeak3.commands.CClientDBFind;
import com.github.theholywaffle.teamspeak3.commands.CClientDBInfo;
import com.github.theholywaffle.teamspeak3.commands.CClientDBList;
import com.github.theholywaffle.teamspeak3.commands.CClientDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CClientEdit;
import com.github.theholywaffle.teamspeak3.commands.CClientFind;
import com.github.theholywaffle.teamspeak3.commands.CClientGetDBIdFromUId;
import com.github.theholywaffle.teamspeak3.commands.CClientGetIds;
import com.github.theholywaffle.teamspeak3.commands.CClientInfo;
import com.github.theholywaffle.teamspeak3.commands.CClientKick;
import com.github.theholywaffle.teamspeak3.commands.CClientList;
import com.github.theholywaffle.teamspeak3.commands.CClientMove;
import com.github.theholywaffle.teamspeak3.commands.CClientPermList;
import com.github.theholywaffle.teamspeak3.commands.CClientPoke;
import com.github.theholywaffle.teamspeak3.commands.CClientSetServerQueryLogin;
import com.github.theholywaffle.teamspeak3.commands.CClientUpdate;
import com.github.theholywaffle.teamspeak3.commands.CComplainAdd;
import com.github.theholywaffle.teamspeak3.commands.CComplainDel;
import com.github.theholywaffle.teamspeak3.commands.CComplainDelAll;
import com.github.theholywaffle.teamspeak3.commands.CComplainList;
import com.github.theholywaffle.teamspeak3.commands.CGM;
import com.github.theholywaffle.teamspeak3.commands.CHostInfo;
import com.github.theholywaffle.teamspeak3.commands.CInstanceEdit;
import com.github.theholywaffle.teamspeak3.commands.CInstanceInfo;
import com.github.theholywaffle.teamspeak3.commands.CLogin;
import com.github.theholywaffle.teamspeak3.commands.CLogout;
import com.github.theholywaffle.teamspeak3.commands.CMessageAdd;
import com.github.theholywaffle.teamspeak3.commands.CMessageDel;
import com.github.theholywaffle.teamspeak3.commands.CMessageGet;
import com.github.theholywaffle.teamspeak3.commands.CMessageList;
import com.github.theholywaffle.teamspeak3.commands.CMessageUpdateFlag;
import com.github.theholywaffle.teamspeak3.commands.CPermFind;
import com.github.theholywaffle.teamspeak3.commands.CPermGet;
import com.github.theholywaffle.teamspeak3.commands.CPermIdGetByName;
import com.github.theholywaffle.teamspeak3.commands.CPermOverview;
import com.github.theholywaffle.teamspeak3.commands.CPermReset;
import com.github.theholywaffle.teamspeak3.commands.CPermissionList;
import com.github.theholywaffle.teamspeak3.commands.CPrivilegeKeyAdd;
import com.github.theholywaffle.teamspeak3.commands.CPrivilegeKeyDelete;
import com.github.theholywaffle.teamspeak3.commands.CPrivilegeKeyList;
import com.github.theholywaffle.teamspeak3.commands.CPrivilegeKeyUse;
import com.github.theholywaffle.teamspeak3.commands.CQuit;
import com.github.theholywaffle.teamspeak3.commands.CSendTextMessage;
import com.github.theholywaffle.teamspeak3.commands.CServerCreate;
import com.github.theholywaffle.teamspeak3.commands.CServerDelete;
import com.github.theholywaffle.teamspeak3.commands.CServerEdit;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupAdd;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupAddClient;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupAutoAddPerm;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupAutoDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupClientList;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupCopy;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupDel;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupDelClient;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupDelPerm;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupList;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupPermList;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupRename;
import com.github.theholywaffle.teamspeak3.commands.CServerGroupsByClientId;
import com.github.theholywaffle.teamspeak3.commands.CServerIdGetByPort;
import com.github.theholywaffle.teamspeak3.commands.CServerInfo;
import com.github.theholywaffle.teamspeak3.commands.CServerList;
import com.github.theholywaffle.teamspeak3.commands.CServerNotifyRegister;
import com.github.theholywaffle.teamspeak3.commands.CServerNotifyUnregister;
import com.github.theholywaffle.teamspeak3.commands.CServerProcessStop;
import com.github.theholywaffle.teamspeak3.commands.CServerRequestConnectionInfo;
import com.github.theholywaffle.teamspeak3.commands.CServerSnapshotCreate;
import com.github.theholywaffle.teamspeak3.commands.CServerSnapshotDeploy;
import com.github.theholywaffle.teamspeak3.commands.CServerStart;
import com.github.theholywaffle.teamspeak3.commands.CServerStop;
import com.github.theholywaffle.teamspeak3.commands.CSetClientChannelGroup;
import com.github.theholywaffle.teamspeak3.commands.CUse;
import com.github.theholywaffle.teamspeak3.commands.CVersion;
import com.github.theholywaffle.teamspeak3.commands.CWhoAmI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TS3Api
{
  private final TS3Query query;
  
  public TS3Api(TS3Query query)
  {
    this.query = query;
  }
  
  public int addBan(String ip, String name, String uid, long timeInSeconds, String reason)
  {
    if ((ip == null) && (name == null) && (uid == null)) {
      throw new IllegalArgumentException("Either IP, Name or UID must be set");
    }
    CBanAdd add = new CBanAdd(ip, name, uid, timeInSeconds, reason);
    if (this.query.doCommand(add)) {
      return add.getFirstResponse().getInt("banid");
    }
    return -1;
  }
  
  public boolean addChannelClientPermission(int channelId, int clientDBId, String permName, int permValue)
  {
    CChannelClientAddPerm add = new CChannelClientAddPerm(channelId, clientDBId, permName, permValue);
    return this.query.doCommand(add);
  }
  
  public int addChannelGroup(String name)
  {
    return addChannelGroup(name, null);
  }
  
  public int addChannelGroup(String name, PermissionGroupDatabaseType type)
  {
    CChannelGroupAdd add = new CChannelGroupAdd(name, type);
    if (this.query.doCommand(add)) {
      return add.getFirstResponse().getInt("cgid");
    }
    return -1;
  }
  
  public boolean addChannelGroupPermission(int groupId, String permName, int permValue)
  {
    CChannelGroupAddPerm add = new CChannelGroupAddPerm(groupId, permName, permValue);
    return this.query.doCommand(add);
  }
  
  public boolean addChannelPermission(int channelId, String permName, int permValue)
  {
    CChannelAddPerm perm = new CChannelAddPerm(channelId, permName, permValue);
    return this.query.doCommand(perm);
  }
  
  public boolean addClientPermission(int clientDBId, String permName, int value, boolean skipped)
  {
    CClientAddPerm add = new CClientAddPerm(clientDBId, permName, value, skipped);
    return this.query.doCommand(add);
  }
  
  public boolean addClientToServerGroup(int groupId, int clientDatabaseId)
  {
    CServerGroupAddClient add = new CServerGroupAddClient(groupId, clientDatabaseId);
    return this.query.doCommand(add);
  }
  
  public boolean addComplaint(int clientDBId, String message)
  {
    CComplainAdd add = new CComplainAdd(clientDBId, message);
    return this.query.doCommand(add);
  }
  
  public boolean addPermissionToAllServerGroups(ServerGroupType type, String permName, int value, boolean negated, boolean skipped)
  {
    CServerGroupAutoAddPerm add = new CServerGroupAutoAddPerm(type, permName, value, negated, skipped);
    return this.query.doCommand(add);
  }
  
  public String addPrivilegeKey(TokenType type, int groupId, int channelId, String description)
  {
    CPrivilegeKeyAdd add = new CPrivilegeKeyAdd(type, groupId, channelId, description);
    if (this.query.doCommand(add)) {
      return add.getFirstResponse().get("token");
    }
    return null;
  }
  
  public String addPrivilegeKeyChannelGroup(int channelGroupId, int channelId, String description)
  {
    return addPrivilegeKey(TokenType.CHANNEL_GROUP, channelGroupId, channelId, description);
  }
  
  public String addPrivilegeKeyServerGroup(int serverGroupId, String description)
  {
    return addPrivilegeKey(TokenType.SERVER_GROUP, serverGroupId, 0, description);
  }
  
  public int addServerGroup(String name)
  {
    return addServerGroup(name, PermissionGroupDatabaseType.REGULAR);
  }
  
  public int addServerGroup(String name, PermissionGroupDatabaseType type)
  {
    CServerGroupAdd add = new CServerGroupAdd(name, type);
    if (this.query.doCommand(add)) {
      return add.getFirstResponse().getInt("sgid");
    }
    return -1;
  }
  
  public boolean addServerGroupPermission(int groupId, String permName, int value, boolean negated, boolean skipped)
  {
    CServerGroupAddPerm add = new CServerGroupAddPerm(groupId, permName, value, negated, skipped);
    return this.query.doCommand(add);
  }
  
  public void addTS3Listeners(TS3Listener... listeners)
  {
    this.query.getEventManager().addListeners(listeners);
  }
  
  public int[] banClient(int clientId, long timeInSeconds)
  {
    return banClient(clientId, timeInSeconds, null);
  }
  
  public int[] banClient(int clientId, long timeInSeconds, String reason)
  {
    CBanClient client = new CBanClient(clientId, timeInSeconds, reason);
    if (this.query.doCommand(client))
    {
      List<Wrapper> response = client.getResponse();
      int banId1 = ((Wrapper)response.get(0)).getInt("banid");
      int banId2 = ((Wrapper)response.get(1)).getInt("banid");
      return new int[] { banId1, banId2 };
    }
    return null;
  }
  
  public int[] banClient(int clientId, String reason)
  {
    return banClient(clientId, 0L, reason);
  }
  
  public boolean broadcast(String message)
  {
    CGM broadcast = new CGM(message);
    return this.query.doCommand(broadcast);
  }
  
  public boolean copyChannelGroup(int sourceGroupId, int targetGroupId, PermissionGroupDatabaseType type)
  {
    if (targetGroupId <= 0) {
      throw new IllegalArgumentException("To create a new channel group, use the method with a String argument");
    }
    CChannelGroupCopy copy = new CChannelGroupCopy(sourceGroupId, targetGroupId, type);
    return this.query.doCommand(copy);
  }
  
  public int copyChannelGroup(int sourceGroupId, String targetName, PermissionGroupDatabaseType type)
  {
    CChannelGroupCopy copy = new CChannelGroupCopy(sourceGroupId, targetName, type);
    if (this.query.doCommand(copy)) {
      return copy.getFirstResponse().getInt("cgid");
    }
    return -1;
  }
  
  public boolean copyServerGroup(int sourceGroupId, int targetGroupId, PermissionGroupDatabaseType type)
  {
    if (targetGroupId <= 0) {
      throw new IllegalArgumentException("To create a new server group, use the method with a String argument");
    }
    CServerGroupCopy copy = new CServerGroupCopy(sourceGroupId, targetGroupId, type);
    return this.query.doCommand(copy);
  }
  
  public int copyServerGroup(int sourceGroupId, String targetName, PermissionGroupDatabaseType type)
  {
    CServerGroupCopy copy = new CServerGroupCopy(sourceGroupId, targetName, type);
    if (this.query.doCommand(copy)) {
      return copy.getFirstResponse().getInt("sgid");
    }
    return -1;
  }
  
  public int createChannel(String name, Map<ChannelProperty, String> options)
  {
    CChannelCreate create = new CChannelCreate(name, options);
    if (this.query.doCommand(create)) {
      return create.getFirstResponse().getInt("cid");
    }
    return -1;
  }
  
  public CreatedVirtualServer createServer(String name, Map<VirtualServerProperty, String> options)
  {
    CServerCreate create = new CServerCreate(name, options);
    if (this.query.doCommand(create)) {
      return new CreatedVirtualServer(create.getFirstResponse().getMap());
    }
    return null;
  }
  
  public Snapshot createServerSnapshot()
  {
    CServerSnapshotCreate create = new CServerSnapshotCreate();
    if (this.query.doCommand(create)) {
      return new Snapshot(create.getRaw());
    }
    return null;
  }
  
  public boolean deleteAllBans()
  {
    CBanDelAll del = new CBanDelAll();
    return this.query.doCommand(del);
  }
  
  public boolean deleteAllComplaints(int clientDBId)
  {
    CComplainDelAll del = new CComplainDelAll(clientDBId);
    return this.query.doCommand(del);
  }
  
  public boolean deleteBan(int banId)
  {
    CBanDel del = new CBanDel(banId);
    return this.query.doCommand(del);
  }
  
  public boolean deleteChannel(int channelId)
  {
    return deleteChannel(channelId, true);
  }
  
  public boolean deleteChannel(int channelId, boolean force)
  {
    CChannelDelete del = new CChannelDelete(channelId, force);
    return this.query.doCommand(del);
  }
  
  public boolean deleteChannelClientPermission(int channelId, int clientDBId, String permName)
  {
    CChannelClientDelPerm del = new CChannelClientDelPerm(channelId, clientDBId, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deleteChannelGroup(int groupId)
  {
    return deleteChannelGroup(groupId, true);
  }
  
  public boolean deleteChannelGroup(int groupId, boolean force)
  {
    CChannelGroupDel del = new CChannelGroupDel(groupId, force);
    return this.query.doCommand(del);
  }
  
  public boolean deleteChannelGroupPermission(int groupId, String permName)
  {
    CChannelGroupDelPerm del = new CChannelGroupDelPerm(groupId, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deleteChannelPermission(int channelId, String permName)
  {
    CChannelDelPerm del = new CChannelDelPerm(channelId, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deleteClientPermission(int clientDBId, String permName)
  {
    CClientDelPerm del = new CClientDelPerm(clientDBId, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deleteComplaint(int targetClientDBId, int fromClientDBId)
  {
    CComplainDel del = new CComplainDel(targetClientDBId, fromClientDBId);
    return this.query.doCommand(del);
  }
  
  public boolean deleteDatabaseClientProperties(int clientDBId)
  {
    CClientDBDelete del = new CClientDBDelete(clientDBId);
    return this.query.doCommand(del);
  }
  
  public boolean deleteOfflineMessage(int messageId)
  {
    CMessageDel del = new CMessageDel(messageId);
    return this.query.doCommand(del);
  }
  
  public boolean deletePermissionFromAllServerGroups(ServerGroupType type, String permName)
  {
    CServerGroupAutoDelPerm del = new CServerGroupAutoDelPerm(type, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deletePrivilegeKey(String token)
  {
    CPrivilegeKeyDelete del = new CPrivilegeKeyDelete(token);
    return this.query.doCommand(del);
  }
  
  public boolean deleteServer(int serverId)
  {
    CServerDelete delete = new CServerDelete(serverId);
    return this.query.doCommand(delete);
  }
  
  public boolean deleteServerGroup(int groupId)
  {
    return deleteServerGroup(groupId, true);
  }
  
  public boolean deleteServerGroup(int groupId, boolean force)
  {
    CServerGroupDel del = new CServerGroupDel(groupId, force);
    return this.query.doCommand(del);
  }
  
  public boolean deleteServerGroupPermission(int groupId, String permName)
  {
    CServerGroupDelPerm del = new CServerGroupDelPerm(groupId, permName);
    return this.query.doCommand(del);
  }
  
  public boolean deployServerSnapshot(Snapshot snapshot)
  {
    return deployServerSnapshot(snapshot.get());
  }
  
  public boolean deployServerSnapshot(String snapshot)
  {
    CServerSnapshotDeploy deploy = new CServerSnapshotDeploy(snapshot);
    return this.query.doCommand(deploy);
  }
  
  public boolean editChannel(int channelId, Map<ChannelProperty, String> options)
  {
    CChannelEdit edit = new CChannelEdit(channelId, options);
    return this.query.doCommand(edit);
  }
  
  public boolean editClient(int clientId, Map<ClientProperty, String> options)
  {
    CClientEdit edit = new CClientEdit(clientId, options);
    return this.query.doCommand(edit);
  }
  
  public boolean editDatabaseClient(int clientDBId, Map<ClientProperty, String> options)
  {
    CClientDBEdit edit = new CClientDBEdit(clientDBId, options);
    return this.query.doCommand(edit);
  }
  
  public boolean editInstance(ServerInstanceProperty property, String value)
  {
    if (!property.isChangeable()) {
      throw new IllegalArgumentException("Property is not changeable");
    }
    CInstanceEdit edit = new CInstanceEdit(property, value);
    return this.query.doCommand(edit);
  }
  
  public boolean editServer(Map<VirtualServerProperty, String> options)
  {
    CServerEdit edit = new CServerEdit(options);
    return this.query.doCommand(edit);
  }
  
  public List<Ban> getBans()
  {
    CBanList list = new CBanList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Ban> bans = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        bans.add(new Ban(response.getMap()));
      }
      return bans;
    }
    return null;
  }
  
  public List<Binding> getBindings()
  {
    CBindingList list = new CBindingList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Binding> bindings = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        bindings.add(new Binding(response.getMap()));
      }
      return bindings;
    }
    return null;
  }
  
  public Channel getChannelByNameExact(String name, boolean ignoreCase)
  {
    String caseName = ignoreCase ? name.toLowerCase() : name;
    List<Channel> allChannels = getChannels();
    if (allChannels == null) {
      return null;
    }
    for (Channel channel : allChannels)
    {
      String channelName = ignoreCase ? channel.getName().toLowerCase() : channel.getName();
      if (caseName.equals(channelName)) {
        return channel;
      }
    }
    return null;
  }
  
  public List<Channel> getChannelsByName(String name)
  {
    CChannelFind find = new CChannelFind(name);
    List<Channel> allChannels = getChannels();
    if (allChannels == null) {
      return null;
    }
    if (this.query.doCommand(find))
    {
        int channelId;
      List<Wrapper> responses = find.getResponse();
      List<Channel> channels = new ArrayList(responses.size());
      for (Wrapper response : responses)
      {
        channelId = response.getInt("cid");
        for (Channel channel : allChannels) {
          if (channel.getId() == channelId)
          {
            channels.add(channel);
            break;
          }
        }
      }
      return channels;
    }
    return null;
  }
  
  public List<Permission> getChannelClientPermissions(int channelId, int clientDBId)
  {
    CChannelClientPermList list = new CChannelClientPermList(channelId, clientDBId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Permission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new Permission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<ChannelGroupClient> getChannelGroupClients(int channelId, int clientDBId, int groupId)
  {
    CChannelGroupClientList list = new CChannelGroupClientList(channelId, clientDBId, groupId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<ChannelGroupClient> clients = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        clients.add(new ChannelGroupClient(response.getMap()));
      }
      return clients;
    }
    return null;
  }
  
  public List<ChannelGroupClient> getChannelGroupClientsByChannelGroupId(int groupId)
  {
    return getChannelGroupClients(-1, -1, groupId);
  }
  
  public List<ChannelGroupClient> getChannelGroupClientsByChannelId(int channelId)
  {
    return getChannelGroupClients(channelId, -1, -1);
  }
  
  public List<ChannelGroupClient> getChannelGroupClientsByClientDBId(int clientDBId)
  {
    return getChannelGroupClients(-1, clientDBId, -1);
  }
  
  public List<Permission> getChannelGroupPermissions(int groupId)
  {
    CChannelGroupPermList list = new CChannelGroupPermList(groupId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Permission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new Permission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<ChannelGroup> getChannelGroups()
  {
    CChannelGroupList list = new CChannelGroupList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<ChannelGroup> groups = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        groups.add(new ChannelGroup(response.getMap()));
      }
      return groups;
    }
    return null;
  }
  
  public ChannelInfo getChannelInfo(int channelId)
  {
    CChannelInfo info = new CChannelInfo(channelId);
    if (this.query.doCommand(info)) {
      return new ChannelInfo(channelId, info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public List<Permission> getChannelPermissions(int channelId)
  {
    CChannelPermList list = new CChannelPermList(channelId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Permission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new Permission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<Channel> getChannels()
  {
    CChannelList list = new CChannelList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Channel> channels = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        channels.add(new Channel(response.getMap()));
      }
      return channels;
    }
    return null;
  }
  
  public Client getClientByNameExact(String name, boolean ignoreCase)
  {
    String caseName = ignoreCase ? name.toLowerCase() : name;
    List<Client> allClients = getClients();
    if (allClients == null) {
      return null;
    }
    for (Client client : allClients)
    {
      String clientName = ignoreCase ? client.getNickname().toLowerCase() : client.getNickname();
      if (caseName.equals(clientName)) {
        return client;
      }
    }
    return null;
  }
  
  public List<Client> getClientsByName(String name)
  {
    CClientFind find = new CClientFind(name);
    List<Client> allClients = getClients();
    if (allClients == null) {
      return null;
    }
    Wrapper response;
    if (this.query.doCommand(find))
    {
      List<Wrapper> responses = find.getResponse();
      List<Client> clients = new ArrayList(responses.size());
      for (Iterator i$ = responses.iterator(); i$.hasNext();)
      {
        response = (Wrapper)i$.next();
        for (Client client : allClients) {
          if (client.getId() == response.getInt("clid"))
          {
            clients.add(client);
            break;
          }
        }
      }
      return clients;
    }
    return null;
  }
  
  public ClientInfo getClientByUId(String clientUId)
  {
    CClientGetIds get = new CClientGetIds(clientUId);
    if (this.query.doCommand(get)) {
      return getClientInfo(get.getFirstResponse().getInt("clid"));
    }
    return null;
  }
  
  public ClientInfo getClientInfo(int clientId)
  {
    CClientInfo info = new CClientInfo(clientId);
    if (this.query.doCommand(info)) {
      return new ClientInfo(clientId, info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public List<Permission> getClientPermissions(int clientDBId)
  {
    CClientPermList list = new CClientPermList(clientDBId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Permission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new Permission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<Client> getClients()
  {
    CClientList list = new CClientList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Client> clients = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        clients.add(new Client(response.getMap()));
      }
      return clients;
    }
    return null;
  }
  
  public List<Complaint> getComplaints()
  {
    return getComplaints(-1);
  }
  
  public List<Complaint> getComplaints(int clientDBId)
  {
    CComplainList list = new CComplainList(clientDBId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Complaint> complaints = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        complaints.add(new Complaint(response.getMap()));
      }
      return complaints;
    }
    return null;
  }
  
  public ConnectionInfo getConnectionInfo()
  {
    CServerRequestConnectionInfo info = new CServerRequestConnectionInfo();
    if (this.query.doCommand(info)) {
      return new ConnectionInfo(info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public List<DatabaseClientInfo> getDatabaseClientsByName(String name)
  {
    CClientDBFind find = new CClientDBFind(name, false);
    if (this.query.doCommand(find))
    {
      List<Wrapper> responses = find.getResponse();
      List<DatabaseClientInfo> clients = new ArrayList(responses.size());
      for (Wrapper response : responses)
      {
        int databaseId = response.getInt("cldbid");
        DatabaseClientInfo clientInfo = getDatabaseClientInfo(databaseId);
        if (clientInfo != null) {
          clients.add(clientInfo);
        }
      }
      return clients;
    }
    return null;
  }
  
  public DatabaseClientInfo getDatabaseClientByUId(String clientUId)
  {
    CClientGetDBIdFromUId get = new CClientGetDBIdFromUId(clientUId);
    if (this.query.doCommand(get)) {
      return getDatabaseClientInfo(get.getFirstResponse().getInt("cldbid"));
    }
    return null;
  }
  
  public DatabaseClientInfo getDatabaseClientInfo(int clientDBId)
  {
    CClientDBInfo info = new CClientDBInfo(clientDBId);
    if (this.query.doCommand(info)) {
      return new DatabaseClientInfo(info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public List<DatabaseClient> getDatabaseClients()
  {
    CClientDBList countList = new CClientDBList(0, 1, true);
    if (this.query.doCommand(countList))
    {
      int count = countList.getFirstResponse().getInt("count");
      List<DatabaseClient> clients = new ArrayList(count);
      
      int i = 0;
      while (i < count)
      {
        CClientDBList list = new CClientDBList(i, 200, false);
        if (this.query.doCommand(list)) {
          for (Wrapper response : list.getResponse()) {
            clients.add(new DatabaseClient(response.getMap()));
          }
        }
        i += 200;
      }
      return clients;
    }
    return null;
  }
  
  public HostInfo getHostInfo()
  {
    CHostInfo info = new CHostInfo();
    if (this.query.doCommand(info)) {
      return new HostInfo(info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public InstanceInfo getInstanceInfo()
  {
    CInstanceInfo info = new CInstanceInfo();
    if (this.query.doCommand(info)) {
      return new InstanceInfo(info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public String getOfflineMessage(int messageId)
  {
    CMessageGet get = new CMessageGet(messageId);
    if (this.query.doCommand(get)) {
      return get.getFirstResponse().get("message");
    }
    return null;
  }
  
  public String getOfflineMessage(Message message)
  {
    return getOfflineMessage(message.getId());
  }
  
  public List<Message> getOfflineMessages()
  {
    CMessageList list = new CMessageList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Message> msg = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        msg.add(new Message(response.getMap()));
      }
      return msg;
    }
    return null;
  }
  
  public List<AdvancedPermission> getPermissionAssignments(String permName)
  {
    CPermFind find = new CPermFind(permName);
    if (this.query.doCommand(find))
    {
      List<Wrapper> responses = find.getResponse();
      List<AdvancedPermission> assignments = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        assignments.add(new AdvancedPermission(response.getMap()));
      }
      return assignments;
    }
    return null;
  }
  
  public int getPermissionIdByName(String permName)
  {
    CPermIdGetByName get = new CPermIdGetByName(permName);
    if (this.query.doCommand(get)) {
      return get.getFirstResponse().getInt("permid");
    }
    return -1;
  }
  
  public List<AdvancedPermission> getPermissionOverview(int channelId, int clientDBId)
  {
    CPermOverview overview = new CPermOverview(channelId, clientDBId);
    if (this.query.doCommand(overview))
    {
      List<Wrapper> responses = overview.getResponse();
      List<AdvancedPermission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new AdvancedPermission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<PermissionInfo> getPermissions()
  {
    CPermissionList list = new CPermissionList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<PermissionInfo> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new PermissionInfo(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public int getPermissionValue(String permName)
  {
    CPermGet get = new CPermGet(permName);
    if (this.query.doCommand(get)) {
      return get.getFirstResponse().getInt("permvalue");
    }
    return -1;
  }
  
  public List<PrivilegeKey> getPrivilegeKeys()
  {
    CPrivilegeKeyList list = new CPrivilegeKeyList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<PrivilegeKey> keys = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        keys.add(new PrivilegeKey(response.getMap()));
      }
      return keys;
    }
    return null;
  }
  
  public List<ServerGroupClient> getServerGroupClients(int serverGroupId)
  {
    CServerGroupClientList list = new CServerGroupClientList(serverGroupId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<ServerGroupClient> clients = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        clients.add(new ServerGroupClient(response.getMap()));
      }
      return clients;
    }
    return null;
  }
  
  public List<ServerGroupClient> getServerGroupClients(ServerGroup serverGroup)
  {
    return getServerGroupClients(serverGroup.getId());
  }
  
  public List<Permission> getServerGroupPermissions(int serverGroupId)
  {
    CServerGroupPermList list = new CServerGroupPermList(serverGroupId);
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<Permission> permissions = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        permissions.add(new Permission(response.getMap()));
      }
      return permissions;
    }
    return null;
  }
  
  public List<Permission> getServerGroupPermissions(ServerGroup serverGroup)
  {
    return getServerGroupPermissions(serverGroup.getId());
  }
  
  public List<ServerGroup> getServerGroups()
  {
    CServerGroupList list = new CServerGroupList();
    if (this.query.doCommand(list))
    {
      List<Wrapper> responses = list.getResponse();
      List<ServerGroup> groups = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        groups.add(new ServerGroup(response.getMap()));
      }
      return groups;
    }
    return null;
  }
  
  public List<ServerGroup> getServerGroupsByClientId(int clientDatabaseId)
  {
    CServerGroupsByClientId client = new CServerGroupsByClientId(clientDatabaseId);
    if (this.query.doCommand(client))
    {
        Wrapper response;
      List<Wrapper> responses = client.getResponse();
      List<ServerGroup> list = new ArrayList(responses.size());
      List<ServerGroup> allGroups = getServerGroups();
      for (Iterator i$ = responses.iterator(); i$.hasNext();)
      {
        response = (Wrapper)i$.next();
        for (ServerGroup s : allGroups) {
          if (s.getId() == response.getInt("sgid")) {
            list.add(s);
          }
        }
      }
      return list;
    }
    return null;
  }
  
  public List<ServerGroup> getServerGroupsByClient(Client client)
  {
    return getServerGroupsByClientId(client.getDatabaseId());
  }
  
  public int getServerIdByPort(int port)
  {
    CServerIdGetByPort s = new CServerIdGetByPort(port);
    if (this.query.doCommand(s)) {
      return s.getFirstResponse().getInt("server_id");
    }
    return -1;
  }
  
  public VirtualServerInfo getServerInfo()
  {
    CServerInfo info = new CServerInfo();
    if (this.query.doCommand(info)) {
      return new VirtualServerInfo(info.getFirstResponse().getMap());
    }
    return null;
  }
  
  public Version getVersion()
  {
    CVersion version = new CVersion();
    if (this.query.doCommand(version)) {
      return new Version(version.getFirstResponse().getMap());
    }
    return null;
  }
  
  public List<VirtualServer> getVirtualServers()
  {
    CServerList serverList = new CServerList();
    if (this.query.doCommand(serverList))
    {
      List<Wrapper> responses = serverList.getResponse();
      List<VirtualServer> servers = new ArrayList(responses.size());
      for (Wrapper response : responses) {
        servers.add(new VirtualServer(response.getMap()));
      }
      return servers;
    }
    return null;
  }
  
  public boolean kickClientFromChannel(int... clientIds)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_CHANNEL, null, clientIds);
  }
  
  public boolean kickClientFromChannel(Client... clients)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_CHANNEL, null, clients);
  }
  
  public boolean kickClientFromChannel(String message, int... clientIds)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_CHANNEL, message, clientIds);
  }
  
  public boolean kickClientFromChannel(String message, Client... clients)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_CHANNEL, message, clients);
  }
  
  public boolean kickClientFromServer(int... clientIds)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_SERVER, null, clientIds);
  }
  
  public boolean kickClientFromServer(Client... clients)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_SERVER, null, clients);
  }
  
  public boolean kickClientFromServer(String message, int... clientIds)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_SERVER, message, clientIds);
  }
  
  public boolean kickClientFromServer(String message, Client... clients)
  {
    return kickClients(ReasonIdentifier.REASON_KICK_SERVER, message, clients);
  }
  
  private boolean kickClients(ReasonIdentifier reason, String message, Client... clients)
  {
    int[] clientIds = new int[clients.length];
    for (int i = 0; i < clients.length; i++) {
      clientIds[i] = clients[i].getId();
    }
    return kickClients(reason, message, clientIds);
  }
  
  private boolean kickClients(ReasonIdentifier reason, String message, int... clientIds)
  {
    CClientKick kick = new CClientKick(reason, message, clientIds);
    return this.query.doCommand(kick);
  }
  
  public boolean login(String username, String password)
  {
    CLogin login = new CLogin(username, password);
    return this.query.doCommand(login);
  }
  
  public boolean logout()
  {
    CLogout logout = new CLogout();
    return this.query.doCommand(logout);
  }
  
  public boolean moveChannel(int channelId, int channelTargetId)
  {
    return moveChannel(channelId, channelTargetId, 0);
  }
  
  public boolean moveChannel(int channelId, int channelTargetId, int order)
  {
    CChannelMove move = new CChannelMove(channelId, channelTargetId, order);
    return this.query.doCommand(move);
  }
  
  public boolean moveClient(int channelId)
  {
    return moveClient(channelId, null);
  }
  
  public boolean moveClient(ChannelBase channel)
  {
    return moveClient(channel.getId(), null);
  }
  
  public boolean moveClient(int channelId, String channelPassword)
  {
    return moveClient(0, channelId, channelPassword);
  }
  
  public boolean moveClient(ChannelBase channel, String channelPassword)
  {
    return moveClient(0, channel.getId(), channelPassword);
  }
  
  public boolean moveClient(int clientId, int channelId)
  {
    return moveClient(clientId, channelId, null);
  }
  
  public boolean moveClient(Client client, ChannelBase channel)
  {
    return moveClient(client.getId(), channel.getId(), null);
  }
  
  public boolean moveClient(int clientId, int channelId, String channelPassword)
  {
    CClientMove move = new CClientMove(clientId, channelId, channelPassword);
    return this.query.doCommand(move);
  }
  
  public boolean moveClient(Client client, ChannelBase channel, String channelPassword)
  {
    return moveClient(client.getId(), channel.getId(), channelPassword);
  }
  
  public boolean pokeClient(int clientId, String message)
  {
    CClientPoke poke = new CClientPoke(clientId, message);
    return this.query.doCommand(poke);
  }
  
  @Deprecated
  public boolean quit()
  {
    return this.query.doCommand(new CQuit());
  }
  
  public boolean registerAllEvents()
  {
    boolean success = registerEvent(TS3EventType.SERVER);
    success &= registerEvent(TS3EventType.TEXT_SERVER);
    success &= registerEvent(TS3EventType.CHANNEL, 0);
    success &= registerEvent(TS3EventType.TEXT_CHANNEL, 0);
    success &= registerEvent(TS3EventType.TEXT_PRIVATE);
    
    return success;
  }
  
  public boolean registerEvent(TS3EventType eventType)
  {
    if ((eventType == TS3EventType.CHANNEL) || (eventType == TS3EventType.TEXT_CHANNEL)) {
      return registerEvent(eventType, 0);
    }
    return registerEvent(eventType, -1);
  }
  
  public boolean registerEvent(TS3EventType eventType, int channelId)
  {
    CServerNotifyRegister register = new CServerNotifyRegister(eventType, channelId);
    return this.query.doCommand(register);
  }
  
  public boolean registerEvents(TS3EventType... eventTypes)
  {
    for (TS3EventType type : eventTypes) {
      if (!registerEvent(type)) {
        return false;
      }
    }
    return true;
  }
  
  public boolean removeClientFromServerGroup(int serverGroupId, int clientDatabaseId)
  {
    CServerGroupDelClient del = new CServerGroupDelClient(serverGroupId, clientDatabaseId);
    return this.query.doCommand(del);
  }
  
  public boolean removeClientFromServerGroup(ServerGroup serverGroup, Client client)
  {
    return removeClientFromServerGroup(serverGroup.getId(), client.getDatabaseId());
  }
  
  public void removeTS3Listeners(TS3Listener... listeners)
  {
    this.query.getEventManager().removeListeners(listeners);
  }
  
  public boolean renameChannelGroup(int channelGroupId, String name)
  {
    CChannelGroupRename rename = new CChannelGroupRename(channelGroupId, name);
    return this.query.doCommand(rename);
  }
  
  public boolean renameChannelGroup(ChannelGroup channelGroup, String name)
  {
    return renameChannelGroup(channelGroup.getId(), name);
  }
  
  public boolean renameServerGroup(int serverGroupId, String name)
  {
    CServerGroupRename rename = new CServerGroupRename(serverGroupId, name);
    return this.query.doCommand(rename);
  }
  
  public boolean renameServerGroup(ServerGroup serverGroup, String name)
  {
    return renameChannelGroup(serverGroup.getId(), name);
  }
  
  public String resetPermissions()
  {
    CPermReset reset = new CPermReset();
    if (this.query.doCommand(reset)) {
      return reset.getFirstResponse().get("token");
    }
    return null;
  }
  
  public boolean selectVirtualServerById(int id)
  {
    CUse use = new CUse(id, -1);
    return this.query.doCommand(use);
  }
  
  public boolean selectVirtualServerByPort(int port)
  {
    CUse use = new CUse(-1, port);
    return this.query.doCommand(use);
  }
  
  public boolean selectVirtualServer(VirtualServer server)
  {
    return selectVirtualServerById(server.getId());
  }
  
  public boolean sendOfflineMessage(String clientUId, String subject, String message)
  {
    CMessageAdd add = new CMessageAdd(clientUId, subject, message);
    return this.query.doCommand(add);
  }
  
  public boolean sendTextMessage(TextMessageTargetMode targetMode, int targetId, String message)
  {
    CSendTextMessage msg = new CSendTextMessage(targetMode.getIndex(), targetId, message);
    return this.query.doCommand(msg);
  }
  
  public boolean sendChannelMessage(int channelId, String message)
  {
    return (moveClient(channelId)) && (sendTextMessage(TextMessageTargetMode.CHANNEL, 0, message));
  }
  
  public boolean sendChannelMessage(String message)
  {
    return sendTextMessage(TextMessageTargetMode.CHANNEL, 0, message);
  }
  
  public boolean sendServerMessage(int serverId, String message)
  {
    return (selectVirtualServerById(serverId)) && (sendTextMessage(TextMessageTargetMode.SERVER, 0, message));
  }
  
  public boolean sendServerMessage(String message)
  {
    return sendTextMessage(TextMessageTargetMode.SERVER, 0, message);
  }
  
  public boolean sendPrivateMessage(int clientId, String message)
  {
    return sendTextMessage(TextMessageTargetMode.CLIENT, clientId, message);
  }
  
  public boolean setClientChannelGroup(int groupId, int channelId, int clientDBId)
  {
    CSetClientChannelGroup group = new CSetClientChannelGroup(groupId, channelId, clientDBId);
    return this.query.doCommand(group);
  }
  
  public boolean setMessageRead(int messageId)
  {
    return setMessageReadFlag(messageId, true);
  }
  
  public boolean setMessageRead(Message message)
  {
    return setMessageReadFlag(message.getId(), true);
  }
  
  public boolean setMessageReadFlag(int messageId, boolean read)
  {
    CMessageUpdateFlag flag = new CMessageUpdateFlag(messageId, read);
    if (this.query.doCommand(flag)) {
      return flag.getError().isSuccessful();
    }
    return read;
  }
  
  public boolean setMessageReadFlag(Message message, boolean read)
  {
    return setMessageReadFlag(message.getId(), read);
  }
  
  public boolean setNickname(String nickname)
  {
    Map<ClientProperty, String> options = Collections.singletonMap(ClientProperty.CLIENT_NICKNAME, nickname);
    return updateClient(options);
  }
  
  public boolean startServer(int serverId)
  {
    CServerStart start = new CServerStart(serverId);
    return this.query.doCommand(start);
  }
  
  public boolean startServer(VirtualServer virtualServer)
  {
    return startServer(virtualServer.getId());
  }
  
  public boolean stopServer(int serverId)
  {
    CServerStop stop = new CServerStop(serverId);
    return this.query.doCommand(stop);
  }
  
  public boolean stopServer(VirtualServer virtualServer)
  {
    return stopServer(virtualServer.getId());
  }
  
  public boolean stopServerProcess()
  {
    CServerProcessStop stop = new CServerProcessStop();
    return this.query.doCommand(stop);
  }
  
  public boolean unregisterAllEvents()
  {
    CServerNotifyUnregister unr = new CServerNotifyUnregister();
    return this.query.doCommand(unr);
  }
  
  public boolean updateClient(Map<ClientProperty, String> options)
  {
    CClientUpdate update = new CClientUpdate(options);
    return this.query.doCommand(update);
  }
  
  public String updateServerQueryLogin(String loginName)
  {
    CClientSetServerQueryLogin login = new CClientSetServerQueryLogin(loginName);
    if (this.query.doCommand(login)) {
      return login.getFirstResponse().get("client_login_password");
    }
    return null;
  }
  
  public boolean usePrivilegeKey(String token)
  {
    CPrivilegeKeyUse use = new CPrivilegeKeyUse(token);
    return this.query.doCommand(use);
  }
  
  public boolean usePrivilegeKey(PrivilegeKey privilegeKey)
  {
    return usePrivilegeKey(privilegeKey.getToken());
  }
  
  public ServerQueryInfo whoAmI()
  {
    CWhoAmI whoAmI = new CWhoAmI();
    if (this.query.doCommand(whoAmI)) {
      return new ServerQueryInfo(whoAmI.getFirstResponse().getMap());
    }
    return null;
  }
}
