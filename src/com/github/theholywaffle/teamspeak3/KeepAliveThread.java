package com.github.theholywaffle.teamspeak3;

import com.github.theholywaffle.teamspeak3.commands.CWhoAmI;
import java.net.Socket;
import java.util.logging.Logger;

public class KeepAliveThread
  extends Thread
{
  private static final int SLEEP = 60000;
  private final TS3Query ts3;
  private final SocketWriter writer;
  
  public KeepAliveThread(TS3Query ts3, SocketWriter socketWriter)
  {
    super("[TeamSpeak-3-Java-API] Keep alive");
    this.ts3 = ts3;
    this.writer = socketWriter;
  }
  
  public void run()
  {
    while ((this.ts3.getSocket() != null) && (this.ts3.getSocket().isConnected()) && (this.ts3.getOut() != null) && (!isInterrupted()))
    {
      long idleTime = this.writer.getIdleTime();
      if (idleTime >= 60000L) {
        this.ts3.doCommand(new CWhoAmI());
      } else {
        try
        {
          Thread.sleep(60000L - idleTime);
        }
        catch (InterruptedException e)
        {
          interrupt();
          break;
        }
      }
    }
    if (!isInterrupted()) {
      TS3Query.log.warning("KeepAlive thread has stopped!");
    }
  }
}
