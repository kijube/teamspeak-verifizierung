package at.kijube.tsverifier.bukkit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.kijube.tsverifier.bukkit.utils.BukkitPlayer;
import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.utils.TSCommandHandler;

public class TeamspeakCommand implements CommandExecutor {

	private ICore core;
	public TeamspeakCommand(ICore core) {
		this.core = core;
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(cs instanceof Player) {
			Player p = (Player) cs;
			TSCommandHandler.handle(new BukkitPlayer(p), args, core);
		}
		return true;
	}

}
