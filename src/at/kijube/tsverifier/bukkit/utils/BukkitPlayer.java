package at.kijube.tsverifier.bukkit.utils;

import java.util.UUID;

import org.bukkit.entity.Player;

import at.kijube.tsverifier.interfaces.IPlayer;

public class BukkitPlayer implements IPlayer {
	
	private Player p;
	public BukkitPlayer(Player p) {
		this.p = p;
	}

	@Override
	public UUID getUUID() {
		return p.getUniqueId();
	}

	@Override
	public void sendMessage(String message) {
		p.sendMessage(message);
	}

}
