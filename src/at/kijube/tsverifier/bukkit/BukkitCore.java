package at.kijube.tsverifier.bukkit;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import at.kijube.tsverifier.bukkit.commands.TeamspeakCommand;
import at.kijube.tsverifier.bukkit.utils.BukkitAsyncMySQL;
import at.kijube.tsverifier.bukkit.utils.BukkitPlayer;
import at.kijube.tsverifier.interfaces.IAsyncMySQL;
import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.interfaces.IPlayer;
import at.kijube.tsverifier.listener.TeamspeakListener;
import at.kijube.tsverifier.teamspeak.TeamspeakData;
import at.kijube.tsverifier.teamspeak.TeamspeakManager;
import at.kijube.tsverifier.utils.Config;
import at.kijube.tsverifier.utils.Metrics;

public class BukkitCore extends JavaPlugin implements ICore {
	
	private Config conf_ts, conf_ts_msg, conf_msg, conf_mysql;
	
	private TeamspeakManager teamspeakManager;
	private IAsyncMySQL mysql;
	
	@Override
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		this.getCommand("teamspeak").setExecutor(new TeamspeakCommand(this));
		conf_ts = new Config(getConfig(), "teamspeak");
		conf_ts_msg = new Config(getConfig(), "messages.teamspeak");
		conf_msg = new Config(getConfig(), "messages");
		conf_mysql = new Config(getConfig(), "mysql");
		mysql = new BukkitAsyncMySQL(this, conf_mysql.getString("host"), (int) conf_mysql.getObject("port"), conf_mysql.getString("username"), conf_mysql.getString("password"), conf_mysql.getString("database"));
		teamspeakManager = new TeamspeakManager(this, new TeamspeakData(conf_ts.getString("host"), conf_ts.getString("queryname"), conf_ts.getString("querypassword"), conf_ts.getString("bot-nickname"), (int) conf_ts.getObject("port"), (int) conf_ts.getObject("virtualserver"), (int) conf_ts.getObject("promote-group-id")));
		listen();
		
		System.out.println("###########################");
		System.out.println("   TeamspeakVerifer v" + this.getDescription().getVersion());
		System.out.println("  - by kijube");
		System.out.println("###########################");
		
	    try {
	        Metrics metrics = new Metrics(this);
	        metrics.start();
	    } catch (IOException e) {
	        // Failed to submit the stats :-(
	    }
	}
	
	@Override
	public void onDisable() {
		getTeamspeakManager().disconnect();
	}
	
	@Override
	public TeamspeakManager getTeamspeakManager() {
		return teamspeakManager;
	}
	
	@Override
	public IAsyncMySQL getMySQL() {
		return mysql;
	}

	@Override
	public Config getMessageConfig() {
		return conf_msg;
	}

	@Override
	public Config getTeamspeakConfig() {
		return conf_ts;
	}

	@Override
	public Config getMySQLConfig() {
		return conf_mysql;
	}

	@Override
	public Config getTeamspeakMessageConfig() {
		return conf_ts_msg;
	}

	@Override
	public boolean isOnline(String name) {
		return Bukkit.getPlayer(name) != null && Bukkit.getPlayer(name).isOnline();
	}

	@Override
	public UUID getUUID(String name) {
		return Bukkit.getPlayer(name).getUniqueId();
	}

	@Override
	public void listen() {
		new TeamspeakListener(this);
	}
	
	@Override
	public IPlayer getPlayer(String uuid) {
		return new BukkitPlayer(Bukkit.getPlayer(UUID.fromString(uuid)));
	}
	

}
