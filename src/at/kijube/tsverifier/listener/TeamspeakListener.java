package at.kijube.tsverifier.listener;

import java.sql.SQLException;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.teamspeak.TeamspeakManager;
import at.kijube.tsverifier.utils.KeyGenerator;
import net.md_5.bungee.api.ChatColor;

public class TeamspeakListener {

	private TS3Api api;
	private ICore core;

	public TeamspeakListener (ICore core) {
		this.api = core.getTeamspeakManager().getTS3Api();
		this.core = core;
		listen();
	}

	private void listen() {
		api.registerAllEvents();
		api.addTS3Listeners(new TS3Listener() {

			@Override
			public void onTextMessage(TextMessageEvent e) {
				if(e.getTargetMode() == TextMessageTargetMode.CLIENT || e.getTargetMode() == TextMessageTargetMode.CHANNEL) {
					String msg = e.getMessage().toLowerCase().trim();
					if(msg.startsWith("!verify")) {
						
						for(int i : api.getClientByUId(e.getInvokerUniqueId()).getServerGroups()) {
							if(i == core.getTeamspeakManager().getData().getPromoteGroup()) return;
						}
						
						if(msg.split(" ").length < 1) {
							api.sendPrivateMessage(e.getInvokerId(), core.getTeamspeakMessageConfig().getString("cmd-verify-information-required"));
						} else {
							String name = msg.split(" ")[1];

							if(core.isOnline(name)) {
								String uuid = core.getUUID(name).toString();
								
								core.getMySQL().query(core.getMySQL().prepare(TeamspeakManager.SQL_CHECK_VERIFY.replace("%uuid%", uuid)), rs -> {
									try {
										if(!rs.next()) {
											String code = KeyGenerator.randomKey();
											api.sendPrivateMessage(e.getInvokerId(), core.getTeamspeakMessageConfig().getString("cmd-verify").replace("%code%", code));
											core.getTeamspeakManager().requestVerification(uuid, e.getInvokerUniqueId(), code);
											if(core.getPlayer(uuid) != null) {
												core.getPlayer(uuid).sendMessage(ChatColor.translateAlternateColorCodes('&', core.getMessageConfig().getString("teamspeak-client-requested-verification").replace("%name%", api.getClientByUId(e.getInvokerUniqueId()).getNickname()).replace("%code%", code)));
											}
										} else {
											api.sendPrivateMessage(e.getInvokerId(), core.getTeamspeakMessageConfig().getString("uuid-already-verified"));
										}
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								});
							} else {
								api.sendPrivateMessage(e.getInvokerId(), core.getTeamspeakMessageConfig().getString("cmd-verify-player-not-online"));
							}
						}
					}
				}

			}

			@Override
			public void onClientJoin(ClientJoinEvent e) {

				for(int g : api.getClientByNameExact(e.getClientNickname(), false).getServerGroups()) {
					if(g == core.getTeamspeakManager().getData().getPromoteGroup()) return;
				}
				api.sendPrivateMessage(e.getClientId(), core.getTeamspeakMessageConfig().getString("welcome-message-non-verified"));
			}






			/*
			 * The rest is unused
			 */

			@Override
			public void onServerEdit(ServerEditedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onClientMoved(ClientMovedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onClientLeave(ClientLeaveEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelPasswordChanged(ChannelPasswordChangedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelMoved(ChannelMovedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelEdit(ChannelEditedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelDeleted(ChannelDeletedEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChannelCreate(ChannelCreateEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

}
