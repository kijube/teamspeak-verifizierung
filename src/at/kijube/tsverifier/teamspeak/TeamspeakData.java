package at.kijube.tsverifier.teamspeak;

public class TeamspeakData {
	
	private String host, username, password, nickname, table;
	private int port, virtualServer, promoteRank;
	
	
	public TeamspeakData(String host, String username, String password, String nickname, int port, int virtualServer, int promoteRank) {
		this.username = username;
		this.password = password;
		this.nickname = nickname;
		this.promoteRank = promoteRank;
		this.table = "teamspeak";
		this.port = port;
		this.virtualServer = virtualServer;
	}
	
	public String getHost() {
		return host;
	}
	
	public String getTable() {
		return table;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public int getPort() {
		return port;
	}
	
	public int getVirtualServer() {
		return virtualServer;
	}

	public int getPromoteGroup() {
		return promoteRank;
	}

}
