package at.kijube.tsverifier.teamspeak;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;

import at.kijube.tsverifier.interfaces.ICore;

public class TeamspeakManager {

	private ICore core;
	private TeamspeakData data;
	private TS3Api api;
	private TS3Query query;
	private TS3Config config;

	//The verification keys, String1: minecraft uuid, String2: key
	private Map<String, String> verificationKeys = new HashMap<String, String>();

	//The verification uids, String1: key, String2: teamspeak uid
	private Map<String, String> verificationKeyTeamspeakIDs = new HashMap<String, String>();

	public static String SQL_VERIFY = "INSERT INTO %table% (uuid, clientuid) VALUES ('%uuid%', '%clientUid%')";
	public static String SQL_CHECK_VERIFY = "SELECT * FROM %table% WHERE uuid = '%uuid%'";
	public static String SQL_UNVERIFY = "DELETE FROM %table% WHERE uuid = '%uuid%'";

	public TeamspeakManager(final ICore core, final TeamspeakData data) {
		this.core = core;
		this.data = data;
		SQL_CHECK_VERIFY = SQL_CHECK_VERIFY.replace("%table%", data.getTable());
		SQL_VERIFY = SQL_VERIFY.replace("%table%", data.getTable());
		SQL_UNVERIFY = SQL_UNVERIFY.replace("%table%", data.getTable());

		connect();
	}

	public void disconnect() {
		query.exit();
	}

	public TeamspeakData getData() {
		return data;
	}

	public boolean isVerificationRequested(String mcUUID) {
		return verificationKeys.containsKey(mcUUID);
	}

	public boolean isKeyCorrect(String mcUUID, String key) {
		return verificationKeys.get(mcUUID).equals(key);
	}

	public String getVerificationTeamspeakUID(String key) {
		return verificationKeyTeamspeakIDs.get(key);
	}

	public void requestVerification(String mcUUID, String tsUID, String key) {
		this.verificationKeys.put(mcUUID, key);
		this.verificationKeyTeamspeakIDs.put(key, tsUID);
	}

	public void deleteVerification(String mcUUID) {
		String key = verificationKeys.get(mcUUID);
		verificationKeys.remove(mcUUID);
		if(key == null) return;
		verificationKeyTeamspeakIDs.remove(key);
	}

	public void connect() {
		config = new TS3Config();
		config.setHost(data.getHost());
		config.setLoginCredentials(data.getUsername(), data.getPassword());
		config.setQueryPort(data.getPort());
		config.setDebugLevel(Level.OFF);

		query = new TS3Query(config);
		query.connect();
		api = query.getApi();
		api.setNickname(data.getNickname());
		api.selectVirtualServerById(data.getVirtualServer());
		api.setNickname(data.getNickname());
	}

	public TS3Api getTS3Api() {
		return api;
	}

	public void verify(final String clientUid, final String playerUUId) {
		core.getMySQL().update(core.getMySQL().prepare(SQL_VERIFY.replace("%uuid%", playerUUId).replace("%clientUid%", clientUid)));
		if(api.getClientByUId(clientUid) != null) {
			api.addClientToServerGroup(data.getPromoteGroup(), api.getClientByUId(clientUid).getDatabaseId());
		} else {
			api.addClientToServerGroup(data.getPromoteGroup(), api.getDatabaseClientByUId(clientUid).getDatabaseId());
		}
	}

	public void unverify(final String playerUUId) {
		core.getMySQL().query(core.getMySQL().prepare(SQL_CHECK_VERIFY.replace("%uuid%", playerUUId)), rs -> {
			try {
				if(rs.next()) {
					String uid = rs.getString("clientuid");
						api.removeClientFromServerGroup(data.getPromoteGroup(), api.getDatabaseClientByUId(uid).getDatabaseId());
						api.removeClientFromServerGroup(20, core.getTeamspeakManager().getTS3Api().getDatabaseClientByUId(uid).getDatabaseId());
						api.kickClientFromServer(api.getDatabaseClientByUId(uid).getDatabaseId());
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		core.getMySQL().update(core.getMySQL().prepare(SQL_UNVERIFY.replace("%uuid%", playerUUId)));
	}

}
