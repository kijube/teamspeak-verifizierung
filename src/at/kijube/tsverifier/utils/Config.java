package at.kijube.tsverifier.utils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.config.Configuration;

/**
 * Bukkit FileConfiguration Utility Class
 * Easily load all values from config
 * 
 * @author kijube
 *
 */
public class Config {

	private FileConfiguration cfg;
	private Map<String, Object> settings;
	
	/**
	 * Create a new Config Object from the given FileConfiguration
	 * 
	 * @param cfg The FileConfiguration for the Config Object
	 * @param section The section of the config
	 */
	public Config(FileConfiguration cfg, String section) {
		this.cfg = cfg;
		this.load(section);
	}
	
	/**
	 * Create a new Config object from the given Configuration
	 * @param cfg the configuration
	 * @param section The section of the config
	 */
	public Config(Configuration cfg, String section) {
		settings = new HashMap<>();
		for(String key : cfg.getSection(section).getKeys()) {
			settings.put(key, cfg.get(key));
		}
	}
	
	/**
	 * Load all values from the FileConfiguration Object
	 */
	private void load(String section) {
		settings = cfg.getConfigurationSection(section).getValues(true);
	}
	
	/**
	 * Get a message from the Config
	 * 
	 * @param key The identifier of the message
	 * 
	 * @return The requested message
	 */
	public String getString(String key) {
		return ChatColor.translateAlternateColorCodes('&', (String) settings.get(key));
	}
	
	
	/**
	 * Get a List<?> from the config
	 * 
	 * @param key The identifier of the List
	 * 
	 * @return The requested List
	 */
	public List<?> getList(String key) {
		return (List<?>) settings.get(key);
	}
	
	/**
	 * Get a Object from the config
	 * 
	 * @param key The identifier of the Object
	 * 
	 * @return The requested Object
	 */
	public Object getObject(String key) {
		return settings.get(key);
	}
}