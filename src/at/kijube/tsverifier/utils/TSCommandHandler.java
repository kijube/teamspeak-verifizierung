package at.kijube.tsverifier.utils;

import java.util.List;

import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.interfaces.IPlayer;
import net.md_5.bungee.api.ChatColor;

public class TSCommandHandler {

	@SuppressWarnings("unchecked")
	public static void handle(IPlayer p, String[] args, ICore core) {

		if(args.length == 0) {
			for(String str : (List<String>) core.getMessageConfig().getList("cmd-help")) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', str));
			}
		} else if(args.length == 1) {
			if(args[0].equalsIgnoreCase("unverify")) {
				core.getTeamspeakManager().unverify(p.getUUID().toString());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', core.getMessageConfig().getString("unverify-success")));
			}
		} else if(args.length == 2) {
			if(args[0].equalsIgnoreCase("verify")) {
				String key = args[1];
				if(core.getTeamspeakManager().isVerificationRequested(p.getUUID().toString())) {
					if(core.getTeamspeakManager().isKeyCorrect(p.getUUID().toString(), key)) {
						core.getTeamspeakManager().verify(core.getTeamspeakManager().getVerificationTeamspeakUID(key), p.getUUID().toString());
						core.getTeamspeakManager().deleteVerification(p.getUUID().toString());
						p.sendMessage(core.getMessageConfig().getString("verify-success"));
					} else {
						p.sendMessage(core.getMessageConfig().getString("verify-failed"));
					}
				} else {
					p.sendMessage(core.getMessageConfig().getString("verify-not-requested"));
				}
			}

		}
	}

}
