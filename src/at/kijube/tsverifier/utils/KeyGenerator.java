package at.kijube.tsverifier.utils;

import java.util.Random;

/**
 * Generates a key for verification requests
 * @author kijube
 *
 */
public class KeyGenerator {
	
	private static final char[] POOL = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private static final Random rnd = new Random();
	private static final int LENGTH = 6;
	
	private static char randomChar() {
		return POOL[rnd.nextInt(POOL.length)];
	}
	
	/**
	 * Generate a random key
	 * @return a random key String
	 */
	public static String randomKey() {
		StringBuilder kb = new StringBuilder();
		for(int i = 0; i < LENGTH; i++) {
			kb.append(randomChar());
		}
		return kb.toString();
	}

}
