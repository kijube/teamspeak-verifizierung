package at.kijube.tsverifier.bungee;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.google.common.io.ByteStreams;

import at.kijube.tsverifier.bungee.commands.TeamspeakCommand;
import at.kijube.tsverifier.bungee.utils.BungeeAsyncMySQL;
import at.kijube.tsverifier.bungee.utils.BungeePlayer;
import at.kijube.tsverifier.interfaces.IAsyncMySQL;
import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.interfaces.IPlayer;
import at.kijube.tsverifier.listener.TeamspeakListener;
import at.kijube.tsverifier.teamspeak.TeamspeakData;
import at.kijube.tsverifier.teamspeak.TeamspeakManager;
import at.kijube.tsverifier.utils.Config;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class BungeeCore extends Plugin implements ICore {
	
	private TeamspeakManager teamspeakManager;
	private IAsyncMySQL mysql;
	
	private Config conf_ts, conf_ts_msg, conf_msg, conf_mysql;
	
	@Override
	public void onEnable() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
		
		try {
			conf_ts = new Config(ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")), "teamspeak");
			conf_ts_msg = new Config(ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")), "messages.teamspeak");
			conf_msg = new Config(ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")), "messages");
			conf_mysql = new Config(ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")), "mysql");
			mysql = new BungeeAsyncMySQL(this, conf_mysql.getString("host"), (int) conf_mysql.getObject("port"), conf_mysql.getString("username"), conf_mysql.getString("password"), conf_mysql.getString("database"));
			teamspeakManager = new TeamspeakManager(this, new TeamspeakData(conf_ts.getString("host"), conf_ts.getString("queryname"), conf_ts.getString("querypassword"), conf_ts.getString("bot-nickname"), (int) conf_ts.getObject("port"), (int) conf_ts.getObject("virtualserver"), (int) conf_ts.getObject("promote-group-id")));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getProxy().getPluginManager().registerCommand(this, new TeamspeakCommand(this));
		listen();
		System.out.println("###########################");
		System.out.println("   TeamspeakVerifer v" + this.getDescription().getVersion());
		System.out.println("  - by kijube");
		System.out.println("###########################");
		System.out.println("");
		System.out.println("Make sure to give your users the tsverifier.use permission! Otherwise they won't be able to use the /ts command!");
		System.out.println("");
	}
	
	@Override
	public void onDisable() {
		getTeamspeakManager().disconnect();
	}
	
	@Override
	public TeamspeakManager getTeamspeakManager() {
		return teamspeakManager;
	}
	
	@Override
	public IAsyncMySQL getMySQL() {
		return mysql;
	}


	@Override
	public Config getMessageConfig() {
		return conf_msg;
	}

	@Override
	public Config getTeamspeakConfig() {
		return conf_ts;
	}

	@Override
	public Config getMySQLConfig() {
		return conf_mysql;
	}

	@Override
	public Config getTeamspeakMessageConfig() {
		return conf_ts_msg;
	}

	@Override
	public boolean isOnline(String name) {
		return ProxyServer.getInstance().getPlayer(name) != null && ProxyServer.getInstance().getPlayer(name).isConnected();
	}

	@Override
	public UUID getUUID(String name) {
		return ProxyServer.getInstance().getPlayer(name).getUniqueId();
	}

	@Override
	public void listen() {
		new TeamspeakListener(this);
		
	}

	@Override
	public IPlayer getPlayer(String uuid) {
		return new BungeePlayer(ProxyServer.getInstance().getPlayer(UUID.fromString(uuid)));
	}

}