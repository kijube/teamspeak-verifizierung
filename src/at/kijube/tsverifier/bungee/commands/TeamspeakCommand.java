package at.kijube.tsverifier.bungee.commands;

import at.kijube.tsverifier.bungee.utils.BungeePlayer;
import at.kijube.tsverifier.interfaces.ICore;
import at.kijube.tsverifier.utils.TSCommandHandler;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamspeakCommand extends Command {


	private ICore core;
	public TeamspeakCommand(ICore core) {
		super("teamspeak", "tsverifier.use", new String[]{"ts"});
		this.core = core;
	}

	@Override
	public void execute(CommandSender cs, String[] args) {

		if(cs instanceof ProxiedPlayer) {
			TSCommandHandler.handle(new BungeePlayer((ProxiedPlayer) cs), args, core);
		}

	}

}
