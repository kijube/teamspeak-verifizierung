package at.kijube.tsverifier.bungee.utils;

import java.util.UUID;

import at.kijube.tsverifier.interfaces.IPlayer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeePlayer implements IPlayer {
	
	private ProxiedPlayer p;
	public BungeePlayer(ProxiedPlayer p) {
		this.p = p;
	}

	@Override
	public UUID getUUID() {
		return p.getUniqueId();
	}

	@Override
	public void sendMessage(String message) {
		p.sendMessage(new TextComponent(message));
	}

}
