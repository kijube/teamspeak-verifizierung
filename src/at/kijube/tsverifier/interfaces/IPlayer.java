package at.kijube.tsverifier.interfaces;

import java.util.UUID;


/**
 * Interface for universal use bukkit & bungee
 * @author kijube
 *
 */
public interface IPlayer {
	
	UUID getUUID();
	void sendMessage(String message);
	

}
