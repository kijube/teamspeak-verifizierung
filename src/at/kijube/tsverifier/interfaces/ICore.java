package at.kijube.tsverifier.interfaces;

import java.util.UUID;

import at.kijube.tsverifier.teamspeak.TeamspeakManager;
import at.kijube.tsverifier.utils.Config;

/**
 * Interface for universal use for bukkit and bungee
 * @author kijube
 *
 */
public interface ICore {
	
	/**
	 * Get the AsyncMySQL Object
	 * @return AsyncMySQL
	 */
	IAsyncMySQL getMySQL();
	
	/**
	 * Get the Teamspeak Manager
	 * @return TeamspeakManager
	 */
	TeamspeakManager getTeamspeakManager();
	
	Config getMessageConfig();
	Config getTeamspeakConfig();
	Config getMySQLConfig();
	Config getTeamspeakMessageConfig();
	
	boolean isOnline(String name);
	UUID getUUID(String name);
	
	void listen();
	
	IPlayer getPlayer(String uuid);

}
