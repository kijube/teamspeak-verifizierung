package at.kijube.tsverifier.interfaces;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.function.Consumer;

public interface IAsyncMySQL {
	
	void update(PreparedStatement statement);
	void query(PreparedStatement statement, Consumer<ResultSet> consumer);
	PreparedStatement prepare(String query);
	void createTable();

}
